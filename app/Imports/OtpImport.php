<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Concerns\ToCollection;
use PDF;

class OtpImport implements ToCollection
{

    protected $date;

    public function __construct($date)
    {
        $this->coll_date = $date;
    }

    public function collection(Collection $rows)
    {
        File::makeDirectory(public_path().'/otp/'.$this->coll_date, $mode = 0777, true, true);
        foreach ($rows as $row)
        {
            File::makeDirectory(public_path().'/otp/'.$this->coll_date.'/'.$row[0].'_'.$row[1], $mode = 0777, true, true);
            $files = File::files(public_path().'/otp/');

            foreach($files as $file){
                $row['date'] = $this->coll_date;
                $pos = strpos($file->getFilename(), strval($row[0]));

                if ($pos !== false) {
                    copy(public_path().'/otp/'.$file->getFilename(), public_path().'/otp/'.$this->coll_date.'/'.$row[0].'_'.$row[1].'/'.$file->getFilename());
                    PDF::loadView('frontend.otp-pdf', compact('row'))
                        ->save(public_path().'/otp/'.$this->coll_date.'/'.$row[0].'_'.$row[1].'/'.$row[0].'_'.$row[1].'.pdf');

                    File::delete(public_path().'/otp/'.$file->getFilename());
                }

            }
        }
    }
}