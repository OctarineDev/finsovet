<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'second_name', 'name', 'surname', 'email', 'phone', 'status', 'token_1',
       'type_1', 'price_1', 'amount',
        'quantity_1', 'ko',
        'birthday', 'place_of_birth', 'id_document_number',
        'identity_document_series', 'identity_date_document', 'name_of_issuing_authority', 'tin', 'address_register',
        'street_register', 'address_of_live', 'street_of_live', 'ki', 'check_photo', 'check_photo', 'source' , 'scan' , 'confirm'
    ];

    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'token', 'id');
    }

}
