<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'admin_id', 'add_admin_id', 'token', 'comment', 'subject', 'date', 'time', 'status', 'name', 'phone', 'type'
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'token', 'id');
    }

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'add_admin_id', 'id');
    }
}
