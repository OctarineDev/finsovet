<?php

namespace App\Http\Controllers;


use App\Http\Requests\Frontend\ConfirmKs;
use App\Http\Requests\Frontend\ConfirmNbki;
use App\Mail\ConfirmCustomer;
use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Auth;
use PDF;
use Zipper;
use ZipArchive;
use Illuminate\Support\Facades\Log;


class CustomerController extends Controller
{
    public function confirm(Request $request)
    {
        $customer = Customer::where('email', $request->email)->first();
        if($customer){
            if($customer->password == ''){
                Mail::to($request->email)->send(new ConfirmCustomer($request->email));
                return redirect()->back()->withErrors(['Проверьте Вашу почту.']);
            }else{
                return redirect()->back()->withErrors(['Данная почта уже верифицирована.']);
            }
        }else{
            return redirect()->back()->withErrors(['Указаный email неверный.']);
        }
    }

    public function confirmNbki(ConfirmNbki $request)
    {
        $order = Order::find($request->id);
        if($order->email == Auth::guard('customer')->user()->email){
            Order::where('id', $request->id)->update([
                'name' => $request->name,
                'surname' => $request->surname,
                'second_name' => $request->second_name,
                'birthday' => $request->birthday,
                'place_of_birth' => $request->place_of_birth,
                'id_document_number' => $request->id_document_number,
                'identity_document_series' => $request->identity_document_series,
                'identity_date_document' => $request->identity_date_document,
                'name_of_issuing_authority' => $request->name_of_issuing_authority,
                'tin' => $request->tin,
                'address_register' => $request->address_register,
                'street_register' => $request->street_register,
                'address_of_live' => $request->address_of_live,
                'street_of_live' => $request->street_of_live,
            ]);
            $order = Order::find($request->id);
            if($request->type == 1){
                $pdf = PDF::loadView('frontend.customer.telegram-pdf', compact('order'));
                return $pdf->stream();
            }else{
                $pdf = PDF::loadView('frontend.customer.nbki-pdf', compact('order'));
                return $pdf->stream();
            }
        }else{
            return redirect()->back()->withErrors(['Читер.']);
        }

    }

    public function print($type, $id)
    {
        $order = Order::find($id);
        if($order){
            if($order->email == Auth::guard('customer')->user()->email){
                if($type == 1){
                    $pdf = PDF::loadView('frontend.customer.telegram-pdf', compact('order'));
                    return $pdf->stream();
                }else{
                    $pdf = PDF::loadView('frontend.customer.nbki-pdf', compact('order'));
                    return $pdf->stream();
                }
            }else{
                return redirect()->back()->withErrors(['Читер.']);
            }
        }else{
            return redirect()->back()->withErrors(['Читер.']);
        }
    }

    public function getHistory($id){
        $order =  Order::where('id', $id)->first();
        if($order->email == Auth::guard('customer')->user()->email && $order->tin != ''){
            $pdf = PDF::loadView( 'frontend.customer.nbki-pdf', compact('order'))->save( public_path().'/nbki.pdf' );

            Zipper::zip(public_path().'/nbki.zip')->add(public_path().'/nbki.pdf')->close();
            Zipper::zip(public_path().'/nbki.zip')->remove('empty.png')->close();
            $file = file_get_contents(public_path().'/nbki.zip');
            $data = base64_encode($file);
            $src = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:mFlow">
              <soapenv:Header/>
              <soapenv:Body>
                 <urn:request>
                    <login>svsovet_admin</login>
                    <password>Uhe3n2cP</password>
                    <payload>
                       <first_name>'.$order->name.'</first_name>
                       <last_name>'.$order->surname.'</last_name>
                       <patr_name>'.$order->second_name.'</patr_name>
                       <date_of_birth>'.date("d.m.Y",strtotime($order->birthday)).'</date_of_birth>
                       <place_of_birth>'.$order->place_of_birth.'</place_of_birth>
                       <doc_number>'.$order->id_document_number.'</doc_number>
                       <doc_series>'.$order->identity_document_series.'</doc_series>
                       <doc_issue_date>'.date("d.m.Y",strtotime($order->identity_date_document)).'</doc_issue_date>
                       <doc_issuer>'.$order->name_of_issuing_authority.'</doc_issuer>
                       <reg_address_city>'.$order->name_of_issuing_authority.'</reg_address_city>
                       <reg_address_street>'.$order->street_register.'</reg_address_street>
                       <fact_address_city>'.$order->address_of_live.'</fact_address_city>
                       <fact_address_street>'.$order->street_of_live.'</fact_address_street>
                       <application>'.$data.'</application>
                    </payload>
                 </urn:request>
              </soapenv:Body>
            </soapenv:Envelope>
            ';
            $Curl = curl_init();
            $CurlOptions = array(
                CURLOPT_URL=>'http://195.24.66.136:8181/BPSWeb/ws/ch',
                CURLOPT_POST=>true,
                CURLOPT_POSTFIELDS=>$src,
                CURLOPT_RETURNTRANSFER=>true,
                CURLOPT_HTTPHEADER=>array('Content-Type: text/xml; charset=utf-8',
                    'Content-Length: '.strlen($src))
            );
            curl_setopt_array($Curl, $CurlOptions);
            $response = curl_exec($Curl);

            Zipper::zip(public_path().'/nbki.zip')->add(public_path().'/empty.png')->close();
            Zipper::zip(public_path().'/nbki.zip')->remove('nbki.pdf')->close();

            $xml_parser = xml_parser_create();
            xml_parse_into_struct($xml_parser, $response, $vals);
            xml_parser_free($xml_parser);
            $_tmp='';
            foreach ($vals as $xml_elem) {
                $x_tag=$xml_elem['tag'];
                $x_level=$xml_elem['level'];
                $x_type=$xml_elem['type'];
                if ($x_level!=1 && $x_type == 'close') {
                    if (isset($multi_key[$x_tag][$x_level]))
                        $multi_key[$x_tag][$x_level]=1;
                    else
                        $multi_key[$x_tag][$x_level]=0;
                }
                if ($x_level!=1 && $x_type == 'complete') {
                    if ($_tmp==$x_tag)
                        $multi_key[$x_tag][$x_level]=1;
                    $_tmp=$x_tag;
                }
            }
            foreach ($vals as $xml_elem) {
                $x_tag=$xml_elem['tag'];
                $x_level=$xml_elem['level'];
                $x_type=$xml_elem['type'];
                if ($x_type == 'open')
                    $level[$x_level] = $x_tag;
                $start_level = 1;
                $php_stmt = '$xml_array';
                if ($x_type=='close' && $x_level!=1)
                    $multi_key[$x_tag][$x_level]++;
                while ($start_level < $x_level) {
                    $php_stmt .= '[$level['.$start_level.']]';
                    if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
                        $php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
                    $start_level++;
                }
                $add='';
                if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
                    if (!isset($multi_key2[$x_tag][$x_level]))
                        $multi_key2[$x_tag][$x_level]=0;
                    else
                        $multi_key2[$x_tag][$x_level]++;
                    $add='['.$multi_key2[$x_tag][$x_level].']';
                }
                if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
                    if ($x_type == 'open')
                        $php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
                    else
                        $php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
                    eval($php_stmt_main);
                }
                if (array_key_exists('attributes', $xml_elem)) {
                    if (isset($xml_elem['value'])) {
                        $php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
                        eval($php_stmt_main);
                    }
                    foreach ($xml_elem['attributes'] as $key=>$value) {
                        $php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
                        eval($php_stmt_att);
                    }
                }
            }
            $file_name =  $xml_array['SOAPENV:ENVELOPE']['SOAPENV:BODY']['URN:REPLY']['PAYLOAD']['DOCUMENT'];

            $zip = new ZipArchive();
            $zip->open(public_path().'/result.zip', ZipArchive::OVERWRITE);
            $zip->addFromString('result_nbki.zip', base64_decode($file_name));
            $zip->close();
            Zipper::make(public_path().'/result.zip')->extractTo(public_path());
            Zipper::make(public_path().'/result_nbki.zip')->extractTo(public_path());
            curl_close($Curl);
            return response()->download(public_path().'/report.pdf');
        }else{
            return view('frontend.home');
        }
    }

    public function getKs(ConfirmKs $request){

        $data = [
            "name"=> $request->name,
            "surname"=> $request->surname,
            "patronymic"=> $request->patronymic,
            "birthdate"=> date('Ymd', strtotime($request->birthdate)),
            "passport"=> [
                "serial"=> $request->serial,
                "number"=> $request->number,
                "issue_date"=> date('Ymd', strtotime($request->issue_date))
            ],
            "phone"=> $request->phones,
            "consent_flag"=> "0",
            "date_consent_given"=> date('Ymd'),
            "consent_responsibility_flag"=> "0",
            "response_ids"=> []
        ];

        $data_string = json_encode($data);

        $ch = curl_init('http://195.24.66.136:8383/api/v1/scores/is');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Accept: application/json',
                'X-API-KEY: D6Wicmiosq60lWJEs3U5PzB7zD0Fq29ZDyJ7kdVY9Q2pN2kmd_Ki8BORH73_C65dXpPfDhbBPhCvGt1rpdwI-ogmfBO7spZecLg5crdHqJqgLuPdYHZfqGfsKzSZEunC',
                'Content-Length: ' . strlen($data_string)
            ]
        );

        $result = json_decode(curl_exec($ch));
        if($result->status == false){
            return Redirect::back()->withErrors(['Введенные Вами данные не корректные']);
        }else{
            $order = [];
            Mail::send('mail.send-bankle', $order, function ($message) use ($request) {
                $from = env('MAIL_FROM');
                $message->to($request->emails)->subject('Памятка');
                $message->from($from, 'Finsovet.online');
                $message->attachData(file_get_contents('http://finsovet.online/public/Памятка__Кредитное_Здоровье_.pdf'), 'Памятка__Кредитное_Здоровье_.pdf');
            });
            Log::info($result->request_id.' Отчество посылал вот это из формы ( переменная сюда вставленна верно ) - '.$request->patronymic);
            return view('frontend.customer.view-ks', ['score' => intval($result->data->value)]);
        }
    }

    public function testKsForTg(Request $request){

        $data = [
            "name"=> $request->name,
            "surname"=> $request->surname,
            "patronymic"=> $request->patronymic,
            "birthdate"=> date('Ymd', strtotime($request->birthdate)),
            "passport"=> [
                "serial"=> $request->serial,
                "number"=> $request->number,
                "issue_date"=> date('Ymd', strtotime($request->issue_date))
            ],
            "phone"=> $request->phones,
            "consent_flag"=> "0",
            "date_consent_given"=> date('Ymd'),
            "consent_responsibility_flag"=> "0",
            "response_ids"=> []
        ];

        $data_string = json_encode($data);

        $ch = curl_init('http://195.24.66.136:8383/api/v1/scores/is');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Accept: application/json',
                'X-API-KEY: D6Wicmiosq60lWJEs3U5PzB7zD0Fq29ZDyJ7kdVY9Q2pN2kmd_Ki8BORH73_C65dXpPfDhbBPhCvGt1rpdwI-ogmfBO7spZecLg5crdHqJqgLuPdYHZfqGfsKzSZEunC',
                'Content-Length: ' . strlen($data_string)
            ]
        );

        $result = json_decode(curl_exec($ch));
        if(!isset($result->data->value)){
            return response()->json(['data' => 0]);

        }
        return response()->json(['data' => intval($result->data->value)]);


    }


    public function test1()
    {
        $order = Order::find(19);
        Zipper::zip(public_path().'/nbki.zip')->add(public_path().'/empty1.png')->close();
        Zipper::zip(public_path().'/nbki.zip')->remove('empty.png')->close();
        $file = file_get_contents(public_path().'/nbki.zip');
        $data = base64_encode($file);
        $src = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:mFlow">
              <soapenv:Header/>
              <soapenv:Body>
                 <urn:request>
                    <login>svsovet_admin</login>
                    <password>Uhe3n2cP</password>
                    <payload>
                       <first_name>'.$order->name.'</first_name>
                       <last_name>'.$order->surname.'</last_name>
                       <patr_name>'.$order->second_name.'</patr_name>
                       <date_of_birth>'.date("d.m.Y",strtotime($order->birthday)).'</date_of_birth>
                       <place_of_birth>'.$order->place_of_birth.'</place_of_birth>
                       <doc_number>'.$order->id_document_number.'</doc_number>
                       <doc_series>'.$order->identity_document_series.'</doc_series>
                       <doc_issue_date>'.date("d.m.Y",strtotime($order->identity_date_document)).'</doc_issue_date>
                       <doc_issuer>'.$order->name_of_issuing_authority.'</doc_issuer>
                       <reg_address_city>'.$order->name_of_issuing_authority.'</reg_address_city>
                       <reg_address_street>'.$order->street_register.'</reg_address_street>
                       <fact_address_city>'.$order->address_of_live.'</fact_address_city>
                       <fact_address_street>'.$order->street_of_live.'</fact_address_street>
                       <application>'.$data.'</application>
                    </payload>
                 </urn:request>
              </soapenv:Body>
            </soapenv:Envelope>
            ';
        $Curl = curl_init();
        $CurlOptions = array(
            CURLOPT_URL=>'http://195.24.66.136:8181/BPSWeb/ws/ch',
            CURLOPT_POST=>true,
            CURLOPT_POSTFIELDS=>$src,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_HTTPHEADER=>array('Content-Type: text/xml; charset=utf-8',
                'Content-Length: '.strlen($src))
        );
        curl_setopt_array($Curl, $CurlOptions);
        $response = curl_exec($Curl);

        Zipper::zip(public_path().'/nbki.zip')->add(public_path().'/empty.png')->close();
        Zipper::zip(public_path().'/nbki.zip')->remove('empty1.png')->close();

        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $response, $vals);
        xml_parser_free($xml_parser);
        $_tmp='';
        foreach ($vals as $xml_elem) {
            $x_tag=$xml_elem['tag'];
            $x_level=$xml_elem['level'];
            $x_type=$xml_elem['type'];
            if ($x_level!=1 && $x_type == 'close') {
                if (isset($multi_key[$x_tag][$x_level]))
                    $multi_key[$x_tag][$x_level]=1;
                else
                    $multi_key[$x_tag][$x_level]=0;
            }
            if ($x_level!=1 && $x_type == 'complete') {
                if ($_tmp==$x_tag)
                    $multi_key[$x_tag][$x_level]=1;
                $_tmp=$x_tag;
            }
        }
        foreach ($vals as $xml_elem) {
            $x_tag=$xml_elem['tag'];
            $x_level=$xml_elem['level'];
            $x_type=$xml_elem['type'];
            if ($x_type == 'open')
                $level[$x_level] = $x_tag;
            $start_level = 1;
            $php_stmt = '$xml_array';
            if ($x_type=='close' && $x_level!=1)
                $multi_key[$x_tag][$x_level]++;
            while ($start_level < $x_level) {
                $php_stmt .= '[$level['.$start_level.']]';
                if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
                    $php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
                $start_level++;
            }
            $add='';
            if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
                if (!isset($multi_key2[$x_tag][$x_level]))
                    $multi_key2[$x_tag][$x_level]=0;
                else
                    $multi_key2[$x_tag][$x_level]++;
                $add='['.$multi_key2[$x_tag][$x_level].']';
            }
            if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
                if ($x_type == 'open')
                    $php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
                else
                    $php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
                eval($php_stmt_main);
            }
            if (array_key_exists('attributes', $xml_elem)) {
                if (isset($xml_elem['value'])) {
                    $php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
                    eval($php_stmt_main);
                }
                foreach ($xml_elem['attributes'] as $key=>$value) {
                    $php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
                    eval($php_stmt_att);
                }
            }
        }
        $file_name =  $xml_array['SOAPENV:ENVELOPE']['SOAPENV:BODY']['URN:REPLY']['PAYLOAD']['DOCUMENT'];

        $zip = new ZipArchive();
        $zip->open(public_path().'/result.zip', ZipArchive::OVERWRITE);
        $zip->addFromString('result_nbki.zip', base64_decode($file_name));
        $zip->close();
        Zipper::make(public_path().'/result.zip')->extractTo(public_path());
        Zipper::make(public_path().'/result_nbki.zip')->extractTo(public_path());
        curl_close($Curl);

    }

}
