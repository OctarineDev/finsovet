<?php

namespace App\Http\Controllers;

use App\Imports\OtpImport;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;
use Zipper;
use ZipArchive;
use Excel;
use Maatwebsite\Excel\Readers;

class OtpController extends Controller
{
    public function order(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $request_uri = 'https://go.instantloan.ru/otpmain/ilws/ILRequest?wsdl';
        $xml_body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ilr="http://b2b.instantloan.com/ws/ilr">
   <soapenv:Header/>
   <soapenv:Body>
      <ilr:invoke>
         <ilr:action>getSalaryTransferApplication</ilr:action>
         <ilr:condition>
            <!--Zero or more repetitions:-->
             <ilr:elements>
               <ilr:name>LoginName</ilr:name>
               <ilr:value>freshadvice</ilr:value>
            </ilr:elements>
            <ilr:elements>
               <ilr:name>Password</ilr:name>
               <ilr:value>bveQ7wHg</ilr:value>
            </ilr:elements>
           <ilr:elements>
               <ilr:name>ReportDateFrom</ilr:name>
               <ilr:value>'.date("Y-m-d", strtotime($request->date)).'</ilr:value>
            </ilr:elements>
             <ilr:elements>
               <ilr:name>ReportDateTo</ilr:name>
               <ilr:value>'.date("Y-m-d", strtotime($request->date)).'</ilr:value>
            </ilr:elements>
         </ilr:condition>
      </ilr:invoke>
   </soapenv:Body>
</soapenv:Envelope>';

        $response = $client->request('POST', $request_uri, [
            'headers' => [
                'Content-Type' => 'text/xml'
            ],
            'body'   => $xml_body,
            'verify' => false
        ]);

        $fullstring = $response->getBody()->getContents();

        $parsed = $this->get_string_between($fullstring, '<name>Documents</name><value>', '</value>');
        $zip = new ZipArchive();

        $zip->open(public_path().'/otp/otp.zip', ZipArchive::OVERWRITE);
        $zip->addFromString('otp_result.zip', base64_decode($parsed));
        $zip->close();
        Zipper::make(public_path().'/otp/otp.zip')->extractTo(public_path().'/otp');
        Zipper::make(public_path().'/otp/otp_result.zip')->extractTo(public_path().'/otp');

        Excel::import(new OtpImport($request->date), public_path().'/otp/reestr.csv', null, \Maatwebsite\Excel\Excel::CSV);
        copy(public_path().'/otp/reestr.csv', public_path().'/otp/'.$request->date.'/reestr.csv');

        File::delete(public_path().'/otp/reestr.csv');
    }

    public function statusUpdate()
    {
        $client = new \GuzzleHttp\Client();
        $request_uri = 'https://test.instantloan.ru/otptest/ilws/ILRequest';

        $xml_body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ilr="http://b2b.instantloan.com/ws/ilr">
   <soapenv:Header/>
   <soapenv:Body>
      <ilr:invoke>
         <ilr:action>setSalaryTransferApplicationStatus</ilr:action>
         <ilr:condition>
            <!--Zero or more repetitions:-->
             <ilr:elements>
               <ilr:name>LoginName</ilr:name>
               <ilr:value>freshadvice</ilr:value>
            </ilr:elements>
            <ilr:elements>
               <ilr:name>Password</ilr:name>
               <ilr:value>bveQ7wHg</ilr:value>
            </ilr:elements>
            <ilr:elements>
               <ilr:name>ClientRefusal</ilr:name>
               <ilr:value></ilr:value>
            </ilr:elements>
             <ilr:elements>
               <ilr:name>ApplicationSent</ilr:name>
               <ilr:value>311336071</ilr:value>
            </ilr:elements>
             <ilr:elements>
               <ilr:name>IncorrectApplication</ilr:name>
               <ilr:value></ilr:value>
            </ilr:elements>            
         </ilr:condition>
      </ilr:invoke>
   </soapenv:Body>
</soapenv:Envelope>';

        $response = $client->request('POST', $request_uri, [
            'headers' => [
                'Content-Type' => 'text/xml'
            ],
            'body'   => $xml_body,
            'verify' => false
        ]);

        $fullstring = $response->getBody()->getContents();

        dd($fullstring);
    }


    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }


}
