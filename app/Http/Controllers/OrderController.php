<?php

namespace App\Http\Controllers;


use App\Mail\ContactMessage;
use App\Mail\NewOrderAdmin;
use App\Mail\NewPaymentAdmin;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use PDF;
use YandexCheckout\Client;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    public function order(Request $request)
    {
        if($request->Текст_Сообщения != ''){
            Mail::to('order@finsovet.online')->send(new ContactMessage($request));
            return response()->json([
                'url' => '/'
            ]);
        }elseif($request->tildapayment['products']){
            $price = 0;
            $type = '';
            $quantity = 0;
            $token = 0;
            foreach($request->tildapayment['products'] as $product){
                if($product['name'] == 'Премиум'){
                    $type = $product['name'];
                    $price = $product['price'];
                    $quantity = $product['quantity'];
                    $token =  date('dmY').''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
                }elseif ($product['name'] == 'Базовый'){
                    $type = $product['name'];
                    $price = $product['price'];
                    $quantity = $product['quantity'];
                    $token =  date('dmY').''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
                }elseif ($product['name'] == 'Расширенный'){
                    $type = $product['name'];
                    $price = $product['price'];
                    $quantity = $product['quantity'];
                    $token =  date('dmY').''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
                }
            }
            $order = Order::create([
                'name' => $request->Name,
                'phone' => $request->Phone,
                'email' => $request->Email,
                'amount' => $request->tildapayment['amount'],
                'type_1' => $type,
                'price_1' => $price,
                'token_1' => $token,
                'quantity_1' => $quantity,

            ]);

            $customer = Customer::where('email', $request->Email)->first();
            if(!$customer){
                Customer::create([
                    'email' => $request->Email
                ]);
            }

            $client = new Client();
            $client->setAuth('173269', 'live_99sgLRtVTUGJUG0WQDqV1T_Vq1k0GPCjg8pz7qQXX4Y');
            $payment = $client->createPayment(
                array(
                    'amount' => array(
                        'value' => $request->tildapayment['amount'],
                        'currency' => 'RUB',
                    ),
                    'confirmation' => array(
                        'type' => 'redirect',
                        'return_url' => 'https://finsovet.online/',
                    ),
                    'description' => $order->id,
                    'capture' => true,
                ),
                uniqid('', true)
            );
            Mail::to('order@finsovet.online')->send(new NewOrderAdmin($order));

            return response()->json([
                'url' => $payment->_confirmation->_confirmationUrl
            ]);
        }else{
            Mail::to('order@finsovet.online')->send(new ContactMessage($request));
            return response()->json([
                'url' => '/'
            ]);
        }


    }

    public function orderNewType(Request $request)
    {
        $type = 'ОТП-кредитное здоровье';
        if($request->type == 'otpc'){
            $price = 144;
        }elseif($request->type == 'otpe'){
            $price = 199;
        }elseif($request->type == 'otpve'){
            $price = 269;
        }elseif($request->type == 'dom'){
            $price = 269;
            $type = 'ДОМ.РФ-кредитное здоровье';
        }else{
            abort(404);
        }
        $quantity = 1;
        $token =  date('dmY').''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);

        $order = Order::create([
            'email' => $request->email,
            'name' => 'Заявка с ОТП',
            'amount' => $price,
            'type_1' => $type,
            'price_1' => $price,
            'token_1' => $token,
            'quantity_1' => $quantity,

        ]);

        $customer = Customer::where('email', $request->email)->first();
        if(!$customer){
            Customer::create([
                'email' => $request->email
            ]);
        }

        $client = new Client();
        $client->setAuth('173269', 'live_99sgLRtVTUGJUG0WQDqV1T_Vq1k0GPCjg8pz7qQXX4Y');
        $payment = $client->createPayment(
            array(
                'amount' => array(
                    'value' => $price,
                    'currency' => 'RUB',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => 'https://finsovet.online/',
                ),
                'description' => $order->id,
                'capture' => true,
            ),
            uniqid('', true)
        );
        Mail::to('order@finsovet.online')->send(new NewOrderAdmin($order));

        return redirect($payment->_confirmation->_confirmationUrl);
    }

    public function checkOrder(Request $request)
    {
        if($request['object']['metadata'] &&  $request['object']['metadata']['bank'] == 1){
            $url = 'http://partnergui.finsovet.online/checkPayment';
            $curl = curl_init($url);
            $data = array('id' => $request['object']['description']);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
            $order = unserialize($request['object']['metadata']['order']);
            $order_1 = unserialize($request['object']['metadata']['order_1']);
            if($order['order_type'] == 'Финсовет Базовый'){
                $order['order_type'] = 'Базовый';
            }elseif($order['order_type'] == 'Финсовет Расширенный'){
                $order['order_type'] = 'Расширенный';
            }else{
                $order['order_type'] = 'Премиум';
            }

            Order::create([
                'name' => $order['name'],
                'email' => $order['email'],
                'phone' => $order_1['phone'],
                'birthday' => $order['birthday'],
                'place_of_birth' => $order['place_of_birth'],
                'id_document_number' => $order['id_document_number'],
                'identity_document_series' => $order['identity_document_series'],
                'identity_date_document' => $order['identity_date_document'],
                'ki' => $order_1['ki'],
                'name_of_issuing_authority' => $order_1['name_of_issuing_authority'],
                'tin' => $order_1['tin'],
                'address_register' => $order_1['address_register'],
                'street_register' => $order_1['street_register'],
                'address_of_live' => $order_1['address_of_live'],
                'street_of_live' => $order_1['street_of_live'],
                'status' => 'Оплачен',
                'amount' => $order_1['amount'],
                'price_1' => $order_1['amount'],
                'confirm' => $order_1['confirm'],

                'scan' => $order_1['scan'],
                'check_photo' => $order_1['check_photo'],
                'token_1' => $order['token'],
                'type_1' => $order['order_type'],
                'source' => 'Банк',
                'quantity_1' => 1,
            ]);
        }else{
            $order = Order::where('id', $request['object']['description'])->first();
            if($order != null){
                if($order->status != 'Оплачен'){
                    Mail::to('order@finsovet.online')->send(new NewPaymentAdmin($order));
                    Order::where('id', $request['object']['description'])->update([
                        'status' => 'Оплачен'
                    ]);

                    $order_for_pdf = $order;
                    $order = ['order' => $order];
                    if($order_for_pdf->type_1 == 'ОТП-кредитное здоровье' || $order_for_pdf->type_1 == 'ДОМ.РФ-кредитное здоровье'){
                        Mail::send('mail.send-pdf', $order, function ($message) use ($order_for_pdf) {
                            $from = env('MAIL_FROM');
                            $pdf = PDF::loadView('frontend.sertification_type_4', compact('order_for_pdf'));
                            $message->to($order_for_pdf->email)->subject('Сертификат');
                            $message->from($from, 'Finsovet.online');
                            $message->attachData($pdf->output(), 'Сертификат Финсовет.pdf');
                            $message->attachData(file_get_contents('http://finsovet.online/public/Памятка__Кредитное_Здоровье_.pdf'), 'Памятка__Кредитное_Здоровье_.pdf');
                        });
                    }else{
                        if ($order_for_pdf->type_1 == 'Базовый'){
                            Mail::send('mail.send-pdf', $order, function ($message) use ($order_for_pdf) {
                                $from = env('MAIL_FROM');
                                $pdf = PDF::loadView('frontend.sertification_type_1', compact('order_for_pdf'));
                                $message->to($order_for_pdf->email)->subject('Сертификат');
                                $message->from($from, 'Finsovet.online');
                                $message->attachData($pdf->output(), 'БАЗОВЫЙ.pdf');
                            });
                        }
                        if ($order_for_pdf->type_1 == 'Расширенный'){
                            Mail::send('mail.send-pdf', $order, function ($message) use ($order_for_pdf) {
                                $from = env('MAIL_FROM');
                                $pdf = PDF::loadView('frontend.sertification_type_2', compact('order_for_pdf'));
                                $message->to($order_for_pdf->email)->subject('Сертификат');
                                $message->from($from, 'Finsovet.online');
                                $message->attachData($pdf->output(), 'РАСШИРЕННЫЙ.pdf');
                            });
                        }
                        if ($order_for_pdf->type_1 == 'Премиум'){
                            Mail::send('mail.send-pdf', $order, function ($message) use ($order_for_pdf) {
                                $from = env('MAIL_FROM');
                                $pdf = PDF::loadView('frontend.sertification_type_3', compact('order_for_pdf'));
                                $message->to($order_for_pdf->email)->subject('Сертификат');
                                $message->from($from, 'Finsovet.online');
                                $message->attachData($pdf->output(), 'ПРЕМИУМ.pdf');
                            });
                        }
                    }
                }
            }
        }
    }

    public function bankLe(Request $request){

        $token =  date('dmY').''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);

        $order = Order::create([
            'name' => $request->last_name.' '.$request->first_name.' '.$request->middle_name,
            'phone' => $request->phone,
            'email' => $request->email,
            'amount' => 0,
            'type_1' => 'ОТП-кредитное здоровье',
            'price_1' => 0,
            'token_1' => $token,
            'quantity_1' => 0,

        ]);
        $customer = Customer::where('email', $request->email)->first();
        if(!$customer){
            Customer::create([
                'email' => $request->email
            ]);
        }

        $order_for_pdf = $order;
        $order = ['order' => $order];

        Mail::send('mail.send-pdf', $order, function ($message) use ($order_for_pdf) {
            $from = env('MAIL_FROM');
            $pdf = PDF::loadView('frontend.sertification_type_4', compact('order_for_pdf'));
            $message->to($order_for_pdf->email)->subject('Сертификат');
            $message->from($from, 'Finsovet.online');
            $message->attachData($pdf->output(), 'Сертификат Финсовет.pdf');
            $message->attachData(file_get_contents('http://finsovet.online/public/Памятка__Кредитное_Здоровье_.pdf'), 'Памятка__Кредитное_Здоровье_.pdf');
        });

    }

}
