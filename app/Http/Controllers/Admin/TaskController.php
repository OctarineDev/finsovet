<?php

namespace App\Http\Controllers\Admin;

use App\Mail\NewTask;
use Mail;
use App\Models\Order;
use App\Models\Admin;
use App\Models\Task;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class TaskController extends Controller
{

    public function create(Request $request)
    {
        if($request->task_id){
            $token = Order::where('token_1', $request->token)->first();
            $task = Task::where('id', $request->task_id)->first();
            if($task->type != $request->type){
                if($token->ko == 'Альфа'){
                    if($token->type_1 == 'Мультиполис'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #1'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #2'){
                        $count = 3;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #3'){
                        $count = 7;
                        $count_wr = 1;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #4'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #5'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 1;
                    }elseif($token->type_1 == 'Телемедицина для Животных МинБанк тестовый сертификат VIP' || $token->type_1 == 'ФинСовет Россельхоз тестовый сертификат VIP' ||
                        $token->type_1 == 'Телемедицина для Животных Россельхоз тестовый сертификат VIP' || $token->type_1 == 'Дачный Вопрос Россельхоз тестовый сертификат VIP' ||
                        $token->type_1 == 'ФинСовет USAFE тестовый сертификат VIP' || $token->type_1 == 'ФинСовет СРАВНИ.РУ тестовый сертификат VIP'){
                        $count = 1;
                    }

                    if ($request->type == 'Письменная'){
                        $tasks = Task::where('token', $token->id)->where('type', 'Письменная')->get();
                        $count = $count_wr;
                    }elseif ($request->type == 'Устная'){
                        $tasks = Task::where('token', $token->id)->where('type', 'Устная')->get();
                        $count = $count;
                    }elseif ($request->type == 'Фин. план'){
                        $tasks = Task::where('token', $token->id)->where('type', 'Фин. план')->get();
                        $count = $count_fp;
                    }
                }else{
                    $tasks = Task::where('token', $token->id)->where('type', 'Устная')->get();
                    if ($request->type == 'Письменная'){
                        return response([
                            'error' => 'У данного сертификата только Устные консультации',
                        ], 422);
                    }elseif ($request->type == 'Фин. план'){
                        return response([
                            'error' => 'У данного сертификата только Устные консультации',
                        ], 422);
                    }
                    if($token->type_1 == 'Премиум'){
                        $count = 12;
                    }elseif($token->type_1 == 'Базовый'){
                        $count = 6;
                    }elseif($token->type_1 == 'Расширенный'){
                        $count = 10;
                    }elseif($token->type_1 == 'РГС 1 ус. конс.'){
                        $count = 1;
                    }elseif($token->type_1 == 'Телемедицина для Животных МинБанк тестовый сертификат VIP' || $token->type_1 == 'ФинСовет Россельхоз тестовый сертификат VIP' ||
                        $token->type_1 == 'Телемедицина для Животных Россельхоз тестовый сертификат VIP' || $token->type_1 == 'Дачный Вопрос Россельхоз тестовый сертификат VIP' ||
                        $token->type_1 == 'ФинСовет USAFE тестовый сертификат VIP' || $token->type_1 == 'ФинСовет СРАВНИ.РУ тестовый сертификат VIP'){
                        $count = 1;
                    }elseif($token->type_1 == 'РЕСО-ОТП'){
                        $count = 1;
                    }
                }
                if(count($tasks) >= $count){
                    return response([
                        'error' => 'Лимит исчерпан',
                    ], 422);
                }
            }


            $admin = Admin::where('name', $request->add_admin_id)->first();
            if(!$admin){
                $admin = Admin::where('email', $request->add_admin_id)->first();
            }
            Task::where('id', $request->task_id)->update([
                'token' => $token->id,
                'name' => $token->surname.' '.$token->name.' '.$token->second_name,
                'phone' => $token->phone,
                'subject' => $request->subject,
                'comment' => $request->comment,
                'date' => $request->date,
                'type' => $request->type,
                'time' => $request->time,
                'add_admin_id' => $admin->id,
                'status' => $request->status,
            ]);

            if($task->add_admin_id != $admin->id){
                if ($task->time == '12'):
                    $time = '09.00-12.00';
                elseif ($task->time == '15'):
                    $time = '12.00-15.00';
                elseif ($task->time == '18'):
                    $time = '15.00-18.00';
                elseif ($task->time == '21'):
                    $time = '18.00-21.00';
                endif;
                Mail::to($admin->email)->send(new NewTask($task));
                file_get_contents('http://95.216.7.246:2776/sendsms?username=real4sms&password=Ld832ikd320askr&from=SOVETMARKET&to='.$admin->phone.'&text=%D0%97%D0%B0+%D0%92%D0%B0%D0%BC%D0%B8+%D0%B1%D1%8B%D0%BB+%D0%B7%D0%B0%D0%BA%D1%80%D0%B5%D0%BF%D0%BB%D0%B5%D0%BD+%D1%82%D0%B0%D1%81%D0%BA+%E2%84%96'.$request->task_id.'+'.$time.'+'.$request->date.'&coding=2&charset=utf-8');
            }

        }else{
            $token = Order::where('token_1', $request->token)->first();
            if($token){
                if($token->ko == 'Альфа'){
                    if($token->type_1 == 'Мультиполис'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #1'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #2'){
                        $count = 3;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #3'){
                        $count = 7;
                        $count_wr = 1;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #4'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 0;
                    }elseif($token->type_1 == 'Страхование имущества #5'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 1;
                    }
                    if ($request->type == 'Письменная'){
                        $tasks = Task::where('token', $token->id)->where('type', 'Письменная')->get();
                        $count = $count_wr;
                    }elseif ($request->type == 'Устная'){
                        $tasks = Task::where('token', $token->id)->where('type', 'Устная')->get();
                        $count = $count;
                    }elseif ($request->type == 'Фин. план'){
                        $tasks = Task::where('token', $token->id)->where('type', 'Фин. план')->get();
                        $count = $count_fp;
                    }
                }else{
                    $tasks = Task::where('token', $token->id)->where('type', 'Устная')->get();
                    if ($request->type == 'Письменная'){
                        return response([
                            'error' => 'У данного сертификата только Устные консультации',
                        ], 422);
                    }elseif ($request->type == 'Фин. план'){
                        return response([
                            'error' => 'У данного сертификата только Устные консультации',
                        ], 422);
                    }
                    if($token->type_1 == 'Премиум'){
                        $count = 12;
                    }elseif($token->type_1 == 'Базовый'){
                        $count = 6;
                    }elseif($token->type_1 == 'Расширенный'){
                        $count = 10;
                    }elseif($token->type_1 == 'РГС 1 ус. конс.'){
                        $count = 1;
                    }elseif($token->type_1 == 'Телемедицина для Животных МинБанк тестовый сертификат VIP' || $token->type_1 == 'ФинСовет Россельхоз тестовый сертификат VIP' ||
                        $token->type_1 == 'Телемедицина для Животных Россельхоз тестовый сертификат VIP' || $token->type_1 == 'Дачный Вопрос Россельхоз тестовый сертификат VIP' ||
                        $token->type_1 == 'ФинСовет USAFE тестовый сертификат VIP' || $token->type_1 == 'ФинСовет СРАВНИ.РУ тестовый сертификат VIP')
                    {
                        $count = 1;
                    }elseif($token->type_1 == 'РЕСО-ОТП'){
                        $count = 1;
                    }
                }

                if(count($tasks) >= $count){
                    return response([
                        'error' => 'Лимит исчерпан',
                    ], 422);
                }
                $admin = Admin::where('name', $request->add_admin_id)->first();
                $task = Task::create([
                    'token' => $token->id,
                    'name' => $token->surname.' '.$token->name.' '.$token->second_name,
                    'phone' => $token->phone,
                    'subject' => $request->subject,
                    'comment' => $request->comment,
                    'date' => $request->date,
                    'type' => $request->type,
                    'time' => $request->time,
                    'admin_id' => Auth::guard('admin')->user()->id,
                    'add_admin_id' => $admin->id,
                    'status' => $request->status,
                ]);

                if ($task->time == '12'):
                    $time = '09.00-12.00';
                elseif ($task->time == '15'):
                    $time = '12.00-15.00';
                elseif ($task->time == '18'):
                    $time = '15.00-18.00';
                elseif ($task->time == '21'):
                    $time = '18.00-21.00';
                endif;

                Mail::to($admin->email)->send(new NewTask($task));
                file_get_contents('http://95.216.7.246:2776/sendsms?username=real4sms&password=Ld832ikd320askr&from=FINSOVET&to=' . $admin->phone . '&text=%D0%97%D0%B0+%D0%92%D0%B0%D0%BC%D0%B8+%D0%B1%D1%8B%D0%BB+%D0%B7%D0%B0%D0%BA%D1%80%D0%B5%D0%BF%D0%BB%D0%B5%D0%BD+%D1%82%D0%B0%D1%81%D0%BA+%E2%84%96' . $task->id . '+' . $time . '+' . $request->date . '&coding=2&charset=utf-8');

            }else{
                return response([
                    'error' => 'Выберите заново сертификат пожалуйста',
                ], 422);
            }


        }

        return response([
            'status' => 'success',
        ], 200);

    }

    public function filter(Request $request){

        if($request->token){
            $token = Order::where('token_1', $request->token)->first();
            $request->token = $token->id;
        }else{
            $request->token = '';
        }
        $query = Task::orderBy('id', 'DESC')->orWhere('id', $request->id)->orWhere('token', $request->token);
        $tasks = $query->get();
        $tasks->load('order');
        return $tasks;
    }

    public function sendMessage(Request $request){
        file_get_contents('http://95.216.7.246:2776/sendsms?username=real4sms&password=Ld832ikd320askr&from=FINSOVET&to='.$request->task['phone'].'&text=%D0%9A+%D1%81%D0%BE%D0%B6%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D1%8E,+%D0%BC%D1%8B+%D0%B4%D0%BE+%D0%92%D0%B0%D1%81+%D0%BD%D0%B5+%D0%B4%D0%BE%D0%B7%D0%B2%D0%BE%D0%BD%D0%B8%D0%BB%D0%B8%D1%81%D1%8C.+%D0%A1%D0%B2%D1%8F%D0%B6%D0%B8%D1%82%D0%B5%D1%81%D1%8C+%D1%81+%D0%BD%D0%B0%D0%BC%D0%B8+88043330503&coding=2&charset=utf-8');
    }

    public function filterStat(Request $request){
        if($request->token){
            $token = Order::where('token_1', $request->token)->first();
            $request->token = $token->id;
        }else{
            $request->token = '';
        }
        $query = Order::where('id', $request->token);
        $orders = $query->get();
        $orders->load('tasks');
        return $orders;
    }

    public function delete($id)
    {
        $order = Task::where('id', $id)->first();
        $order->delete();
        return response([
            'status' => 'success',
        ], 200);
    }

    public function getToken(){
        $orders = Order::orderBy('id', 'DESC')->get();
        $tokens = [];
        foreach($orders as $token){
            $data = (object)[];
            $data->label = $token->token_1;
            $data->value = $token->token_1;
            array_push($tokens, $data);
        }
        return $tokens;
    }
    public function getAdmin(){
        $admins = Admin::orderBy('id', 'DESC')->get();
        $admin_list = [];
        foreach($admins as $admin){
            $data = (object)[];
            $data->label = $admin->name;
            $data->value = $admin->name;
            array_push($admin_list, $data);
        }
        return $admin_list;
    }

}
