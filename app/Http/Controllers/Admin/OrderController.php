<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class OrderController extends Controller
{

    public function create(Request $request)
    {
        if($request->order_id){

            if($request->ki){
                $request->ki = 1;
            }else{
                $request->ki = false;
            }

            Order::where('id', $request->order_id)->update([
                'name' => $request->name,
                'surname' => $request->surname,
                'second_name' => $request->second_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'status' => $request->status,
                'birthday' => $request->birthday,
                'place_of_birth' => $request->place_of_birth,
                'id_document_number' => $request->id_document_number,
                'identity_document_series' => $request->identity_document_series,
                'identity_date_document' => $request->identity_date_document,
                'ki' => $request->ki,
                'ko' => $request->ko,
                'name_of_issuing_authority' => $request->name_of_issuing_authority,
                'tin' => $request->tin,
                'address_register' => $request->address_register,
                'street_register' => $request->street_register,
                'address_of_live' => $request->address_of_live,
                'street_of_live' => $request->street_of_live,
            ]);

            if($request->email){
                $customer = Customer::where('email', $request->email)->first();
                if(!$customer){
                    Customer::create([
                        'email' => $request->email
                    ]);
                }
            }

        }else{
            Order::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'status' => $request->status,
                'birthday' => $request->birthday,
                'place_of_birth' => $request->place_of_birth,
                'id_document_number' => $request->id_document_number,
                'identity_document_series' => $request->identity_document_series,
                'identity_date_document' => $request->identity_date_document,
                'order_type' => $request->order_type,
                'name_of_issuing_authority' => $request->name_of_issuing_authority,
                'tin' => $request->tin,
                'ko' => $request->ko,
                'address_register' => $request->address_register,
                'street_register' => $request->street_register,
                'address_of_live' => $request->address_of_live,
                'street_of_live' => $request->street_of_live,
            ]);
        }

        return response([
            'status' => 'success',
        ], 200);

    }

    public function filter(Request $request){
        $query = Order::orderBy('id', 'DESC')->where('id', '>', 1);

        if($request->ko){
            if($request->ko == 'Магазин/Банк'){
                $query->where('ko', null);
            }else{
                $query->where('ko', $request->ko);
            }
        }
        if($request->token){
            $query->where('token_1', $request->token);
        }
        if($request->order_id){
            $query->where('id', $request->order_id);
        }
        if($request->name){
            $query->where('name', 'LIKE', '%' . $request->name . '%');
        }
        if($request->phone){
            $query->where('phone', 'LIKE', '%' . $request->phone . '%');
        }

        $orders = $query->get();
        $orders->load('tasks');
        return $orders;
    }

    public function delete($id)
    {
        $order = Order::where('id', $id)->first();
        $order->delete();
        return response([
            'status' => 'success',
        ], 200);
    }

    public function apiUpdateOrder(Request $request)
    {
        $order = Order::where('token_1',  $request->token)->first();

        if($order != ''){

            if($request->order_type == 'Финсовет Базовый'){
                $request->order_type = 'Базовый';
            }elseif($request->order_type == 'Финсовет Расширенный'){
                $request->order_type = 'Расширенный';
            }else{
                $request->order_type = 'Премиум';
            }
            Order::where('token_1',  $request->token)->update([
                'name' => $request->name,
                'surname' => $request->surname,
                'second_name' => $request->second_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'birthday' => $request->birthday,
                'place_of_birth' => $request->place_of_birth,
                'id_document_number' => $request->id_document_number,
                'identity_document_series' => $request->identity_document_series,
                'identity_date_document' => $request->identity_date_document,
                'ki' => $request->ki,
                'name_of_issuing_authority' => $request->name_of_issuing_authority,
                'tin' => $request->tin,
                'address_register' => $request->address_register,
                'street_register' => $request->street_register,
                'address_of_live' => $request->address_of_live,
                'street_of_live' => $request->street_of_live,
                'status' => 'Оплачен',
                'amount' => $request->amount,
                'price_1' => $request->amount,
                'confirm' => $request->confirm,
                'scan' => $request->scan,
                'check_photo' => $request->check_photo,
                'token_1' => $request->token,
                'type_1' => $request->order_type,
                'source' => 'Банк',
                'quantity_1' => 1,
            ]);
            if($request->email){
                $customer = Customer::where('email', $request->email)->first();
                if(!$customer){
                    Customer::create([
                        'email' => $request->email
                    ]);
                }
            }

        }else{

            $order = Order::create([
                'name' => $request->name,
                'surname' => $request->surname,
                'second_name' => $request->second_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'birthday' => $request->birthday,
                'place_of_birth' => $request->place_of_birth,
                'id_document_number' => $request->id_document_number,
                'identity_document_series' => $request->identity_document_series,
                'identity_date_document' => $request->identity_date_document,
                'ki' => $request->ki,
                'name_of_issuing_authority' => $request->name_of_issuing_authority,
                'tin' => $request->tin,
                'address_register' => $request->address_register,
                'street_register' => $request->street_register,
                'address_of_live' => $request->address_of_live,
                'street_of_live' => $request->street_of_live,
                'status' => 'Оплачен',
                'amount' => $request->amount,
                'price_1' => $request->amount,
                'confirm' => $request->confirm,
                'scan' => $request->scan,
                'check_photo' => $request->check_photo,
                'token_1' => $request->token,
                'type_1' => $request->order_type,
                'source' => 'Банк',
                'quantity_1' => 1,
            ]);

            if($request->email){
                $customer = Customer::where('email', $request->email)->first();
                if(!$customer){
                    Customer::create([
                        'email' => $request->email
                    ]);
                }
            }
        }
    }

    public function confirm(Request $request)
    {
        $order = Order::where('token_1',  $request->token)->first();

        if($order != ''){
            Order::where('token_1',  $request->token)->update([
                'confirm' => 1
            ]);
        }
    }



}
