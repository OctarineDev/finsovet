<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    public function create(Request $request)
    {
        if($request->admin_id){
            $admin = Admin::find($request->admin_id);
            if ($request->password == $admin->password){
                $password = $request->password;
            }else{
                $password = Hash::make($request->password);
            }
            Admin::where('id', $request->admin_id)->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'role' => $request->role,
                'email' => $request->email,
                'password' => $password,
            ]);
        }else{
            Admin::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'role' => $request->role,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
        }

        return response([
            'status' => 'success',
        ], 200);

    }

    public function delete($id)
    {
        Admin::where('id', $id)->delete();
        return response([
            'status' => 'success',
        ], 200);
    }
}
