<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Frontend\Login;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \Validator;
use Illuminate\Support\Facades\Input;



class CustomerLoginController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('guest:customer');
//    }

    public function showLoginForm()
    {
        $title = 'Авторизация';
        return view('auth.customer-login',compact(['title']));
    }

    public function login(Login $request)
    {
        if(Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return redirect('/customer/profile');
        }else{
            $customer = Customer::where('email', $request->email)->first();
            if($customer->password == ''){
                return redirect()->back()->withErrors(['Для входа в систему пожалуйста верифицируйте Вашу почту.']);
            }else{
                return redirect()->back()->withErrors(['Вы ввели неправильные данные, попробуйте еще раз.']);
            }
        }
    }
}
