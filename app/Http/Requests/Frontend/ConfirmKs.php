<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class ConfirmKs extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:190',
            'surname' => 'required|max:190',
            'patronymic' => 'required|max:190',
            'birthdate' => 'required|max:190',
            'serial' => 'required|max:190',
            'number' => 'required|max:190',
            'issue_date' => 'required|max:190',
            'phones' => 'required|max:190',
            'emails' => 'required|max:190'
        ];
    }
}
