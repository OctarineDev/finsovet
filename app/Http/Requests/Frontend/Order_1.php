<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class Order_1 extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required|string|min:2|max:30',
            'surname' => 'required|string|min:2|max:30',
            'second_name' => 'required|string|min:2|max:30',
            'contract_number' => 'required|string|min:2',
            'phone' => 'required|min:2|max:20',
            'requisites' => 'required|min:2',
            'credit_type' => 'required',
            'date' => 'required',
        ];
    }
}
