<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class ConfirmNbki extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birthday' => 'required|max:190',
            'name' => 'required|max:190',
            'surname' => 'required|max:190',
            'second_name' => 'required|max:190',
            'place_of_birth' => 'required|max:190',
            'id_document_number' => 'required|max:190',
            'identity_document_series' => 'required|max:190',
            'identity_date_document' => 'required|max:190',
            'name_of_issuing_authority' => 'required|max:190',
            'tin' => 'required|max:190',
            'address_register' => 'required|max:190',
            'street_register' => 'required|max:190',
            'address_of_live' => 'required|max:190',
            'street_of_live' => 'required|max:190',

        ];
    }
}
