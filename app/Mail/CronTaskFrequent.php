<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class CronTaskFrequent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($task)
    {
        $this->task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $task = $this->task;
        $from = env('MAIL_FROM');
        return $this->from($from, 'Finsovet.online')->subject('Подходит время выполнения таска №'.$task->id)->view('mail.new-cron-task-frequent-admin')->with(compact(['task']));
    }
}
