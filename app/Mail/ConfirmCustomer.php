<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;
use App\Models\Customer;




class ConfirmCustomer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->email;
        $password= rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
        Customer::where('email', $email)->update([
            'password' => Hash::make($password),
        ]);

        $from = env('MAIL_FROM');
        return $this->from($from, 'Finsovet.online')->subject("Верификация почты")->view('mail.confirm-customer')->with(compact(['password']));
    }
}
