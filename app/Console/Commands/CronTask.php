<?php

namespace App\Console\Commands;

use App\Models\Admin;
use App\Models\Task;
use Illuminate\Console\Command;
use App\Models\Check;
use App\Models\Customer;
use Mail;
use App\Mail\CronTask as NewMail;


class CronTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:sendMessage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отсылаем уведомления, что у таска наступил дедлайн';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::whereIn('status', ['Не выполнен','В работе'])->where('date', date('Y-m-d'))->get();
        if($tasks){
            foreach($tasks as $task){
                $admin = Admin::find($task->add_admin_id);
                if ($task->time == '12'):
                    $time = '09.00-12.00';
                elseif ($task->time == '15'):
                    $time = '12.00-15.00';
                elseif ($task->time == '18'):
                    $time = '15.00-18.00';
                elseif ($task->time == '21'):
                    $time = '18.00-21.00';
                endif;
                Mail::to($admin->email)->send(new NewMail($task));
                file_get_contents('http://95.216.7.246:2776/sendsms?username=real4sms&password=Ld832ikd320askr&from=FINSOVET&to='.$admin->phone.'&text=%D0%9D%D0%B0%D0%BF%D0%BE%D0%BC%D0%B8%D0%BD%D0%B0%D0%B5%D0%BC,+%D1%81%D0%B5%D0%B3%D0%BE%D0%B4%D0%BD%D1%8F+%D0%B4%D0%B5%D0%B4%D0%BB%D0%B0%D0%B9%D0%BD+%D1%82%D0%B0%D1%81%D0%BA%D0%B0+%E2%84%96'.$task->id.'+'.$time.'&coding=2&charset=utf-8');
            }
        }
    }
}
