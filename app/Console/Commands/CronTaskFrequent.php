<?php

namespace App\Console\Commands;

use App\Models\Admin;
use App\Models\Task;
use Illuminate\Console\Command;
use App\Models\Check;
use App\Models\Customer;
use Mail;
use App\Mail\CronTaskFrequent as NewMail;
use Carbon\Carbon;



class CronTaskFrequent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:sendMessageFrequent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отсылаем уведомления, что у таска наступил дедлайн';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('Europe/Moscow');
        $now = Carbon::now();
        $now = intval($now->format("H"));
        $result = $now+4;
        $tasks = Task::whereIn('status', ['Не выполнен','В работе'])->where('date', date('Y-m-d'))->where('time', $result)->get();
        if($tasks){
            foreach($tasks as $task){
                $admin = Admin::find($task->add_admin_id);
                if ($task->time == '12'):
                    $time = '09.00-12.00';
                elseif ($task->time == '15'):
                    $time = '12.00-15.00';
                elseif ($task->time == '18'):
                    $time = '15.00-18.00';
                elseif ($task->time == '21'):
                    $time = '18.00-21.00';
                endif;
                Mail::to($admin->email)->send(new NewMail($task));
                file_get_contents('http://95.216.7.246:2776/sendsms?username=real4sms&password=Ld832ikd320askr&from=FINSOVET&to='.$admin->phone.'&text=%D0%9F%D0%BE%D0%B4%D1%85%D0%BE%D0%B4%D0%B8%D1%82+%D0%B2%D1%80%D0%B5%D0%BC%D1%8F+%D0%B2%D1%8B%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F+%D1%82%D0%B0%D1%81%D0%BA%D0%B0+%E2%84%96'.$task->id.'+'.$time.'&coding=2&charset=utf-8');
            }
        }
    }
}
