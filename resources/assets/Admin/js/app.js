require('./bootstrap');

window.Vue = require('vue');
import VueResource from 'vue-resource';
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)
Vue.use(VueResource);
Vue.use(require('vue-moment'));
Vue.component('login', require('./components/Login.vue'));
Vue.component('admin-menu', require('./components/layouts/Menu.vue'));
Vue.component('order', require('./components/Order.vue'));
Vue.component('task', require('./components/Task.vue'));
Vue.component('admin', require('./components/Admin.vue'));
Vue.component('task-stat', require('./components/TaskStat.vue'));
Vue.component('bz-admins', require('./components/BzAdmin.vue'));
Vue.component('bz-all', require('./components/BzAll.vue'));


Vue.http.headers.common['X-CSRF-TOKEN'] = $("meta[name=token]").attr("value");

const app = new Vue({
    el: '#app'
});
