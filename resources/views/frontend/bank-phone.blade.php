@extends('frontend.includes.header')
@section('content')
    <style>

        @media screen and (max-width: 800px) {
            br{
                display: none;
            }
            .otp-logo{
                margin-left: auto !important;
                margin-right: auto !important;
                display: table;
                margin-bottom: 50px !important;
            }
            .padding-very-big-t{
                padding-top: 50px !important;
            }
            .padding-very-big-b{
                padding-bottom: 50px !important;
            }
            .padding-very-big-b__2{
                padding-bottom: 10px !important;
            }
        }
    </style>
        <img class="otp-logo" src="/public/frontend/images/OTPBank-Logo.png" style="
        max-width: 200px;
        margin-top: 40px;
        margin-left: 50px;
    ">

    <div class="r t-rec custom-section" style="padding-top: 10px;font-family: 'Roboto',Arial,sans-serif;" data-animationappear="off" data-record-type="18">
        <div class="t-container">
            <div class="t-col t-col_12" style="">
                <div class="t-section__title t-title t-title_xs" style="text-align: center;margin-bottom: 70px;" field="btitle">Уважаемый клиент!</div>

                <div class="t051__text t-text t-text_md t503__descr custom-text" style="
                    margin-bottom: 20px;
                    font-size: 27px;
                " field="text">
                    Получите помощь в одобрении кредита. С очень высокой вероятностью с помощью пары простых советов.
                    <br> <br>
                    Всего за 180 руб. вы получите руководство к действию.
                </div>

                <div class="t051__text t-text t-text_md t503__descr custom-text" style="
                    margin-bottom: 20px;
                    font-size: 27px;
                " field="text">
                    Чтобы получить ссылку на инструкцию отправьте <b>SMS</b> на короткий номер <b>7496</b> с текстом: <b>БАНК</b><br>
                    Стоимость отправки SMS: <b>180 рублей</b> (с учетом НДС 20 %).<br>
                    SMS для подтверждения Оператору -  бесплатно<br>
                    <b>Сервис доступен для Операторов России</b>: Билайн, МТС, МегаФон, Tele2.<br>
                    После отправки SMS вы получите сообщение, где будет ссылка на подробные инструкции по улучшению Вашего кредитного здоровья.<br>
                    В случае дополнительных вопросов свяжитесь с нами:  8 800 3013606<br><br>

                </div>

                <div class="t-section__title t-title t-title_xs" style="text-align: center;margin-bottom: 26px;	font-size: 28px;" field="btitle">Добро пожаловать в сервис «ФинСовет» </div>
                <div class="t051__text t-text t-text_md t503__descr" style="font-size: 22px;" field="text">
                    Вместе с Вами мы проанализируем вероятные причины, по которым вам отказали в кредите, а также  <br>
                    разберемся в данной ситуации и предложим действенные рекомендации, которые  <br>
                    помогут исправить положение и вновь обратиться в Банк за получением кредита с  <br>
                    большими шансами на положительный исход.

                </div>
            </div>
        </div>
        <div class="r t-rec custom-section padding-very-big-b__2" style="font-family: 'Roboto',Arial,sans-serif;  padding-bottom: 120px; padding-top: 90px;" data-animationappear="off" data-record-type="18">
            <div class="t503">
                <div class="t-section__container t-container">
                    <div class="t-col t-col_12">
                    </div>
                </div>






                <div class="t-container">
                    <div class="t503__col t-col t-col_4 t-item">
                        <div class="t503__content t-align_center">
                            <div class="t503__wrapper">




                                <img src="/public/frontend/images/lib__tildaicon__37373331-3938-4130-b663-326562383532__cafe_flag.svg" data-original="/public/frontend/images/lib__tildaicon__37373331-3938-4130-b663-326562383532__cafe_flag.svg" class="t503__img t-img loaded" style="width:100px;">
                                <div class="t503__title t-name t-name_md" style="" field="li_title__1476970723554">
                                    Независимо
                                </div>
                            </div>
                            <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476970723554">
                                <div style="font-size:18px;" data-customstyle="yes">
                                    Наш сервис абсолютно нейтрален и <br>
                                    не лоббирует интересы каких-либо <br>
                                    компаний или людей, поэтому мы <br>
                                    объективно подходим к решению <br>
                                    любого вопроса
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="t503__col t-col t-col_4 t-item">
                        <div class="t503__content t-align_center">
                            <div class="t503__wrapper">


                                <img src="/public/frontend/images/Tilda_Icons_43_logistics_selling.svg" data-original="/public/frontend/images/Tilda_Icons_43_logistics_selling.svg" class="t503__img t-img loaded" style="width:100px;">
                                <div class="t503__title t-name t-name_md" style="" field="li_title__1476970709971">
                                    Доступно
                                </div>
                            </div>
                            <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476970709971">
                                <div style="font-size:18px;" data-customstyle="yes">
                                    Наш сервис позволяет понятно <br>
                                    и недорого получить действенный <br>
                                    совет
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="t503__col t-col t-col_4 t-item">
                        <div class="t503__content t-align_center">
                            <div class="t503__wrapper">



                                <img src="/public/frontend/images/Tilda_Icons_1ed_teacher.svg" data-original="/public/frontend/images/Tilda_Icons_1ed_teacher.svg" class="t503__img t-img loaded" style="width:100px;">
                                <div class="t503__title t-name t-name_md" style="" field="li_title__1476970706605">
                                    Профессионально
                                </div>
                            </div>
                            <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476970706605">
                                <div style="font-size:18px;" data-customstyle="yes">
                                    Для ответов на ваши вопросы у нас <br>
                                    работает команда профессионалов. Все <br>
                                    сотрудники являются экспертами в своей <br>
                                    области и имеют многолетний опыт <br>
                                    работы в финансовой сфере

                                </div>
                            </div>
                        </div>
                    </div>
                </div>









            </div>
        </div>
        <div class="r t-rec custom-section padding-very-big-t padding-very-big-b" style="background-color:#eff4f7;font-family: 'Roboto',Arial,sans-serif; padding-bottom: 180px; padding-top: 180px;" data-animationappear="off" data-record-type="18" id="how-work">
            <div class="t-container">
                <div id="rec61246147" class="r t-rec" data-record-type="503" data-bg-color="#eff4f7" data-animationappear="off">
                    <!-- t503 -->
                    <div class="t503">
                        <div class="t-section__container t-container">
                            <div class="t-col t-col_12">
                                <div class="t-section__topwrapper t-align_center">
                                    <div class="t-section__title t-title t-title_xs" field="btitle">Как это работает?</div>
                                </div>
                            </div>
                        </div>










                        <div class="t-container">
                            <div class="t503__col t-col t-col_4 t-item">
                                <div class="t503__content t-align_center">
                                    <div class="t503__wrapper">
                                        <img src="/public/frontend/images/Tilda_Icons_28_tattoo_doc.svg" data-original="/public/frontend/images/Tilda_Icons_28_tattoo_doc.svg" class="t503__img t-img loaded" style="width:100px;">
                                    </div>
                                    <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476961806716">
                                        <div style="font-size:18px;" data-customstyle="yes">
                                            Мы объясним Вам <br>
                                            возможные причины <br>
                                            отказа.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="t503__col t-col t-col_4 t-item">
                                <div class="t503__content t-align_center">
                                    <div class="t503__wrapper">

                                        <img src="/public/frontend/images/Tilda_Icons_17ked_medicine.svg" data-original="/public/frontend/images/Tilda_Icons_17ked_medicine.svg" class="t503__img t-img loaded" style="width:100px;">
                                    </div>
                                    <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476961779840">
                                        <div style="font-size:18px;" data-customstyle="yes">
                                            Вы получите понятные рекомендации как <br>
                                            улучшить ваше кредитное здоровье<br>
                                            чтобы в <br>
                                            последующем получить кредит.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="t503__col t-col t-col_4 t-item">
                                <div class="t503__content t-align_center">
                                    <div class="t503__wrapper">
                                        <img src="/public/frontend/images/Tilda_Icons_19ad_search_result.svg" data-original="/public/frontend/images/Tilda_Icons_19ad_search_result.svg" class="t503__img t-img loaded" style="width:100px;">
                                    </div>
                                    <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476961822275">
                                        <div style="font-size:18px;" data-customstyle="yes">
                                            Воспользуйтесь возможностью <br>
                                            получить Вашу кредитную <br>
                                            историю в Личном кабинете.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>













                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection