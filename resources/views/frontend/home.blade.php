@extends('frontend.includes.header')
@section('content')
    <div id="rec61198874" class="r t-rec" style=" " data-animationappear="off" data-record-type="18">
        <!-- cover -->
        <div class="t-cover" id="recorddiv61198874" bgimgfield="img"
             style="height:90vh; background-image:url('{{ asset('public/frontend/images/tild6661-6632-4162-b931-376136363231__-__resize__20x__Depositphotos_297906.jpg') }}">
            <div class="t-cover__carrier" id="coverCarry61198874" data-content-cover-id="61198874"
                 data-content-cover-bg="{{ asset('public/frontend/images/tild6661-6632-4162-b931-376136363231__depositphotos_297906.jpg') }}"
                 data-content-cover-height="90vh" data-content-cover-parallax=""
                 style="height:90vh;background-attachment:scroll; "></div>
            <div class="t-cover__filter"
                 style="height:90vh;background-image: -moz-linear-gradient(top, rgba(0,0,0,0.60), rgba(0,0,0,0.50));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.60), rgba(0,0,0,0.50));background-image: -o-linear-gradient(top, rgba(0,0,0,0.60), rgba(0,0,0,0.50));background-image: -ms-linear-gradient(top, rgba(0,0,0,0.60), rgba(0,0,0,0.50));background-image: linear-gradient(top, rgba(0,0,0,0.60), rgba(0,0,0,0.50));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#66000000', endColorstr='#7f000000');"></div>
            <div class="t-container">
                <div class="t-col t-col_12 ">
                    <div class="t-cover__wrapper t-valign_middle" style="height:90vh;">
                        <div class="t001 t-align_center">
                            <div class="t001__wrapper" data-hook-content="covercontent">
                                <div class="t001__title t-title t-title_xl t-animate" data-animate-style="fadeinup"
                                     data-animate-group="yes" style="font-size:58px;font-weight:700;" field="title">
                                    Получите полезный совет по любому финансовому вопросу
                                </div>
                                <div class="t001__descr t-descr t-descr_xl t001__descr_center t-animate"
                                     data-animate-style="fadeinup" data-animate-group="yes" style="" field="descr">
                                    Помогаем найти оптимальное финансовое решение
                                </div>
                                <span class="space"></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- arrow -->
            <div class="t-cover__arrow">
                <div class="t-cover__arrow-wrapper t-cover__arrow-wrapper_animated">
                    <div class="t-cover__arrow_mobile">
                        <svg class="t-cover__arrow-svg" style="fill:#ffffff;" x="0px" y="0px" width="38.417px"
                             height="18.592px" viewBox="0 0 38.417 18.592"
                             style="enable-background:new 0 0 38.417 18.592;">
                            <g>
                                <path d="M19.208,18.592c-0.241,0-0.483-0.087-0.673-0.261L0.327,1.74c-0.408-0.372-0.438-1.004-0.066-1.413c0.372-0.409,1.004-0.439,1.413-0.066L19.208,16.24L36.743,0.261c0.411-0.372,1.042-0.342,1.413,0.066c0.372,0.408,0.343,1.041-0.065,1.413L19.881,18.332C19.691,18.505,19.449,18.592,19.208,18.592z"/>
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
            <!-- arrow -->
        </div>
    </div>
    <div id="rec61198875" class="r t-rec t-rec_pt_150 t-rec_pb_150"
         style="padding-top:150px;padding-bottom:150px;background-color:#eff4f7; " data-record-type="474"
         data-bg-color="#eff4f7">
        <!-- T474 -->
        <div class="t474">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t474__descr t-descr t-descr_xxxl t-margin_auto" field="descr" style="">
                        <div style="text-align:center;" data-customstyle="yes">Для ответов на ваши вопросы у нас
                            работает отличная команда профессионалов. Все сотрудники являются экспертами в своей области
                            и имеют многолетний опыт работы в финансовой и страховой сфере, мы сами подберем для вас
                            эксперта в зависимости от профиля вашего вопроса.
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec61290678" class="r t-rec" style=" " data-animationappear="off" data-record-type="651">
        <!-- T651 -->
        <div class="t651" style="">
            <div class="t651__btn" style="">
                <div class="t651__btn_wrapper " style="background:#75d17a;">
                    <svg class="t651__icon" style="fill:#ffffff;" xmlns="http://www.w3.org/2000/svg" width="30px"
                         height="30px" viewBox="0 0 19.3 20.1">
                        <desc>Call</desc>
                        <path d="M4.6 7.6l-.5-.9 2-1.2L4.6 3l-2 1.3-.6-.9 2.9-1.7 2.6 4.1"/>
                        <path d="M9.9 20.1c-.9 0-1.9-.3-2.9-.9-1.7-1-3.4-2.7-4.7-4.8-3-4.7-3.1-9.2-.3-11l.5.9C.2 5.7.4 9.7 3 13.9c1.2 2 2.8 3.6 4.3 4.5 1.1.6 2.7 1.1 4.1.3l1.9-1.2L12 15l-2 1.2c-1.2.7-2.8.3-3.5-.8l-3.2-5.2c-.7-1.2-.4-2.7.8-3.5l.5.9c-.7.4-.9 1.3-.5 2l3.2 5.2c.4.7 1.5.9 2.2.5l2.8-1.7 2.6 4.1-2.8 1.7c-.7.5-1.4.7-2.2.7zM13.7 11.3l-.9-.3c.4-1.1.2-2.2-.4-3.1-.6-1-1.7-1.6-2.8-1.7l.1-1c1.5.1 2.8.9 3.6 2.1.7 1.2.9 2.7.4 4z"/>
                        <path d="M16.5 11.9l-1-.3c.5-1.8.2-3.7-.8-5.3-1-1.6-2.7-2.6-4.7-2.9l.1-1c2.2.3 4.2 1.5 5.4 3.3 1.2 1.9 1.6 4.1 1 6.2z"/>
                        <path d="M18.9 12.5l-1-.3c.7-2.5.2-5.1-1.1-7.2-1.4-2.2-3.7-3.6-6.3-4l.1-1c2.9.4 5.4 2 7 4.4 1.6 2.4 2.1 5.3 1.3 8.1z"/>
                    </svg>
                    <svg class="t651__icon-close" width="16px" height="16px" viewBox="0 0 23 23" version="1.1"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <desc>Close</desc>
                        <g stroke="none" stroke-width="1" fill="#000" fill-rule="evenodd">
                            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="t651__popup">
                <div class="t651__popup-container" style="">
                    <div class="t651__wrapper">
                        <div class="t651__title t-name t-name_xl" style="">Введите свой контактный телефон, и мы
                            свяжемся с вами в ближайшее время!
                        </div>
                        <form id="form61290678" name='form61290678' role="form"
                              action='https://forms.tildacdn.com/procces/' method='POST' data-formactiontype="2"
                              data-inputbox=".t651__blockinput" class="js-form-proccess " data-tilda-captchakey="">
                            <input type="hidden" name="formservices[]" value="4a912a8dd40941278024fef31fa4a868"
                                   class="js-formaction-services">
                            <div class="t651__input-container">
                                <div class="t651__allert-wrapper">
                                    <div class="js-errorbox-all t651__blockinput-errorbox" style="display:none;">
                                        <div class="t651__blockinput-errors-text t-descr t-descr_xs">
                                            <p class="t651__blockinput-errors-item js-rule-error js-rule-error-all"></p>
                                            <p class="t651__blockinput-errors-item js-rule-error js-rule-error-req">
                                                Required field</p>
                                            <p class="t651__blockinput-errors-item js-rule-error js-rule-error-email">
                                                Please correct e-mail address</p>
                                            <p class="t651__blockinput-errors-item js-rule-error js-rule-error-name">
                                                Name Wrong. Correct please</p>
                                            <p class="t651__blockinput-errors-item js-rule-error js-rule-error-phone">
                                                Please correct phone number</p>
                                            <p class="t651__blockinput-errors-item js-rule-error js-rule-error-string">
                                                Please enter letter, number or punctuation symbols.</p>
                                        </div>
                                    </div>
                                    <div class="js-successbox t651__blockinput-success t-text t-text_md"
                                         style="display:none;">
                                        <div class="t651__success-icon">
                                            <svg width="50px" height="50px" viewBox="0 0 50 50">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g fill="#222">
                                                        <path d="M25.0982353,49.2829412 C11.5294118,49.2829412 0.490588235,38.2435294 0.490588235,24.6752941 C0.490588235,11.1064706 11.53,0.0670588235 25.0982353,0.0670588235 C38.6664706,0.0670588235 49.7058824,11.1064706 49.7058824,24.6752941 C49.7058824,38.2441176 38.6664706,49.2829412 25.0982353,49.2829412 L25.0982353,49.2829412 Z M25.0982353,1.83176471 C12.5023529,1.83176471 2.25529412,12.0794118 2.25529412,24.6752941 C2.25529412,37.2705882 12.5023529,47.5182353 25.0982353,47.5182353 C37.6941176,47.5182353 47.9411765,37.2705882 47.9411765,24.6752941 C47.9411765,12.0794118 37.6941176,1.83176471 25.0982353,1.83176471 L25.0982353,1.83176471 Z"></path>
                                                        <path d="M22.8435294,30.5305882 L18.3958824,26.0829412 C18.0511765,25.7382353 18.0511765,25.18 18.3958824,24.8352941 C18.7405882,24.4905882 19.2988235,24.4905882 19.6435294,24.8352941 L22.8429412,28.0347059 L31.7282353,19.1488235 C32.0729412,18.8041176 32.6311765,18.8041176 32.9758824,19.1488235 C33.3205882,19.4935294 33.3205882,20.0517647 32.9758824,20.3964706 L22.8435294,30.5305882 L22.8435294,30.5305882 Z"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="t651__success-message t-descr t-descr_sm">Cпасибо! Мы свяжемся с
                                            вами в ближайшее время.
                                        </div>
                                    </div>
                                </div>
                                <div class="t651__input-wrapper">
                                    <div class="t651__blockinput">
                                        <input type="text" name="phone" class="t651__input t-input js-tilda-rule "
                                               value="" placeholder="+7 (000) 000 0000" data-tilda-req="1"
                                               data-tilda-rule="phone" style=" border:1px solid #d1d1d1; "></div>
                                    <div class="t651__blockbutton">
                                        <button type="submit" class="t651__submit t-submit"
                                                style="background-color:#199c68;text-transform:uppercase;">Позвоните мне
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="t651__additional-info">
                            <div class="t651__text t-descr t-descr_sm" style="">Или Вы можете позвонить нам сами.</div>
                            <div class="t651__phone t-descr t-descr_sm" style="">8 (804) 333 05 03</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    t651_initPopup('61290678');
                }, 500);
            });
        </script>
    </div>
    <div id="rec61198876" class="r t-rec t-rec_pt_150 t-rec_pb_150" style="padding-top:150px;padding-bottom:150px; "
         data-record-type="503">
        <!-- t503 -->
        <div class="t503">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs" field="btitle">Как это работает?</div>
                        <div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">С нами вы можете
                            посоветоваться по любым финансовым вопросам
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="t-container">
                <div class="t503__col t-col t-col_4 t-item">
                    <div class="t503__content t-align_center">
                        <div class="t503__wrapper"><img
                                    src="{{ asset('public/frontend/images/lib__tildaicon__30626337-3965-4632-b666-623130386537__-__empty__Tilda_Icons_28_law_question.svg') }}"
                                    data-original="{{ asset('public/frontend/images/lib__tildaicon__30626337-3965-4632-b666-623130386537__Tilda_Icons_28_law_question.svg') }}"
                                    class="t503__img t-img" imgfield="li_img__1476961779840" style="width:100px;"/>
                            <div class="t503__title t-name t-name_md" style="" field="li_title__1476961779840">У вас
                                есть вопрос?
                            </div>
                        </div>
                        <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476961779840">
                            <div style="font-size:18px;" data-customstyle="yes">Задайте вопрос нашему специалисту прямо
                                сейчас и получите совет по широкому спектру финансовых вопросов
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t503__col t-col t-col_4 t-item">
                    <div class="t503__content t-align_center">
                        <div class="t503__wrapper"><img
                                    src="{{ asset('public/frontend/images/lib__tildaicon__63663564-3530-4730-b866-376431326537__-__empty__touragency_callcentre.svg') }}"
                                    data-original="{{ asset('public/frontend/images/lib__tildaicon__63663564-3530-4730-b866-376431326537__touragency_callcentre.svg') }}"
                                    class="t503__img t-img" imgfield="li_img__1476961806716" style="width:100px;"/>
                            <div class="t503__title t-name t-name_md" style="" field="li_title__1476961806716">У нас
                                есть совет!
                            </div>
                        </div>
                        <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476961806716">
                            <div style="font-size:18px;" data-customstyle="yes">Мы поможем по любым вопросам сбережения,
                                кредитования, инвестирования, страхования и финансово-правового характера
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t503__col t-col t-col_4 t-item">
                    <div class="t503__content t-align_center">
                        <div class="t503__wrapper"><img
                                    src="{{ asset('public/frontend/images/lib__tildaicon__36626236-6632-4134-b530-323138313738__-__empty__25fn_moneybox.svg') }}"
                                    data-original="{{ asset('public/frontend/images/lib__tildaicon__36626236-6632-4134-b530-323138313738__25fn_moneybox.svg') }}"
                                    class="t503__img t-img" imgfield="li_img__1476961822275" style="width:100px;"/>
                            <div class="t503__title t-name t-name_md" style="" field="li_title__1476961822275">
                                Действуйте!
                            </div>
                        </div>
                        <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476961822275">
                            <div style="font-size:18px;" data-customstyle="yes">Воспользуйтесь нашими советами и вы
                                сможете сохранить и даже приумножить свои финансы
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec61293965" class="r t-rec t-rec_pt_90 t-rec_pb_90"
         style="padding-top:90px;padding-bottom:90px;background-color:#75d17a; " data-record-type="37"
         data-bg-color="#75d17a">
        <!-- T022 -->
        <div class="t022 t-align_center">
            <div class="t-container">
                <div class="t-row">
                    <div class="t-col t-col_9 t-prefix_2">
                        <div class="t022__text t-text t-text_sm" style="" field="text">
                            <div style="font-size:26px;" data-customstyle="yes">Наши специалисты дали советы более чем
                                2000+ пользователям.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec61246147" class="r t-rec t-rec_pt_150 t-rec_pb_150"
         style="padding-top:150px;padding-bottom:150px;background-color:#eff4f7; " data-record-type="503"
         data-bg-color="#eff4f7">
        <!-- t503 -->
        <div class="t503">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs" field="btitle">Наши преимущества</div>
                        <div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">С нами вы можете
                            быть спокойны по поводу финансовых вопросов
                        </div>
                    </div>
                </div>
            </div>
            <div class="t-container">
                <div class="t503__col t-col t-col_6 t-item">
                    <div class="t503__content t-align_center">
                        <div class="t503__wrapper"><img
                                    src="{{ asset('public/frontend/images/lib__tildaicon__61393936-3366-4134-b733-353937623064__-__empty__1ed_timer.svg') }}"
                                    data-original="{{ asset('public/frontend/images/lib__tildaicon__61393936-3366-4134-b733-353937623064__1ed_timer.svg') }}"
                                    class="t503__img t-img" imgfield="li_img__1476970706605" style="width:100px;"/>
                            <div class="t503__title t-name t-name_md" style="" field="li_title__1476970706605">
                                Оперативно
                            </div>
                        </div>
                        <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476970706605">
                            <div style="font-size:18px;" data-customstyle="yes">Наши специалисты незамедлительно
                                приступят к решению вашего вопроса и вы получите полезный совет в кратчайшие сроки
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t503__col t-col t-col_6 t-item">
                    <div class="t503__content t-align_center">
                        <div class="t503__wrapper"><img
                                    src="{{ asset('public/frontend/images/lib__tildaicon__63366338-3537-4664-b431-303039353764__-__empty__Tilda_Icons_28_law_consulting.svg') }}"
                                    data-original="{{ asset('public/frontend/images/lib__tildaicon__63366338-3537-4664-b431-303039353764__tilda_icons_28_law_consulting.svg') }}"
                                    class="t503__img t-img" imgfield="li_img__1476970709971" style="width:100px;"/>
                            <div class="t503__title t-name t-name_md" style="" field="li_title__1476970709971">
                                Доступно
                            </div>
                        </div>
                        <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476970709971">
                            <div style="font-size:18px;" data-customstyle="yes">Наш сервис позволяет понятно и недорого
                                получить действенный совет: вы можете позвонить или написать нам
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t-clear t503__separator" style=""></div>
                <div class="t503__col t-col t-col_6 t-item">
                    <div class="t503__content t-align_center">
                        <div class="t503__wrapper"><img
                                    src="{{ asset('public/frontend/images/lib__tildaicon__61633238-3164-4565-a161-616161393462__-__empty__Tilda_Icons_39_IT_structure.svg') }}"
                                    data-original="{{ asset('public/frontend/images/lib__tildaicon__61633238-3164-4565-a161-616161393462__tilda_icons_39_it_structure.svg') }}"
                                    class="t503__img t-img" imgfield="li_img__1476970720067" style="width:100px;"/>
                            <div class="t503__title t-name t-name_md" style="" field="li_title__1476970720067">
                                Вариативно
                            </div>
                        </div>
                        <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476970720067">
                            <div style="font-size:18px;" data-customstyle="yes">Наши специалисты внимательно выслушают
                                ваш вопрос и предложат несколько вариантов решения, поскольку в финансовых вопросах нет
                                единствено правильного ответа
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t503__col t-col t-col_6 t-item">
                    <div class="t503__content t-align_center">
                        <div class="t503__wrapper"><img
                                    src="{{ asset('public/frontend/images/lib__tildaicon__37373331-3938-4130-b663-326562383532__-__empty__cafe_flag.svg') }}"
                                    data-original="{{ asset('public/frontend/images/lib__tildaicon__37373331-3938-4130-b663-326562383532__cafe_flag.svg') }}"
                                    class="t503__img t-img" imgfield="li_img__1476970723554" style="width:100px;"/>
                            <div class="t503__title t-name t-name_md" style="" field="li_title__1476970723554">
                                Независимо
                            </div>
                        </div>
                        <div class="t503__descr t-descr t-descr_xs" style="" field="li_descr__1476970723554">
                            <div style="font-size:18px;" data-customstyle="yes">Наш сервис абсолютно нейтрален и не
                                лоббирует интересы каких-либо компаний или людей, поэтому мы объективно подходим к
                                решению любого вопроса
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec61291558" class="r t-rec t-rec_pt_150 t-rec_pb_30" style="padding-top:150px;padding-bottom:30px; "
         data-animationappear="off" data-record-type="599">
        <!-- T599 -->
        <div class="t599">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs t-animate" data-animate-style="zoomin"
                             data-animate-group="yes" field="btitle">Тарифы
                        </div>
                        <div class="t-section__descr t-descr t-descr_xl t-margin_auto t-animate"
                             data-animate-style="zoomin" data-animate-group="yes" field="bdescr">Для более подробной
                            информации свяжитесь с нами или закажите звонок.
                        </div>
                    </div>
                </div>
            </div>
            <div class="t-container t599__withfeatured">
                <div class="t599__col t-col t-col_4 t-align_center t-animate" data-animate-style="zoomin"
                     data-animate-chain="yes">
                    <div class="t599__content" style="border: 1px solid #e0e6ed; border-radius: 9px;">
                        <div class="t599__title t-name t-name_lg" field="title" style="">БАЗОВЫЙ
                            <br/>
                        </div>
                        <div class="t599__subtitle t-descr t-descr_xxs" field="subtitle" style="">Количество
                            пользователей: 1
                            <br/>
                        </div>
                        <div class="t599__price t-title t-title_xs" field="price" style="">₽990</div>
                        <div class="t599__descr t-descr t-descr_xs" field="descr" style="">
                            <ul>
                                <li> Пакет на год</li>
                                <li>6 обращений</li>
                                <li>Кредитный рейтинг</li>
                                <li></li>
                            </ul>
                        </div>
                        <a href="#order:Базовый =990" onclick="
                        setTimeout(function () {
                            $('.t706__cartwin').addClass('t706__cartwin_showed');
                             $('.type-1').attr('href', '#order')
                             $('.type-1').find('.text-to-cart').text('Уже в корзине')
                        }, 500)" target="" class="t599__btn t-btn t-btn_sm type-1"
                           style="color:#ffffff;background-color:#13ce66;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                            <table style="width:100%; height:100%;">
                                <tr>
                                    <td class="text-to-cart">Купить</td>
                                </tr>
                            </table>
                        </a>
                    </div>
                </div>
                <div class="t599__col t-col t-col_4 t-align_center t599__featured t-animate" data-animate-style="zoomin"
                     data-animate-chain="yes">
                    <div class="t599__content" style="border: 1px solid #e0e6ed; border-radius: 9px;">
                        <div class="t599__title t-name t-name_lg" field="title2" style="">РАСШИРЕННЫЙ
                            <br/>
                        </div>
                        <div class="t599__subtitle t-descr t-descr_xxs" field="subtitle2" style="">Количество
                            пользователей: 1
                            <br/>
                        </div>
                        <div class="t599__price t-title t-title_xs" field="price2" style="">₽1490</div>
                        <div class="t599__descr t-descr t-descr_xs" field="descr2" style="">
                            <ul>
                                <li>Пакет на год</li>
                                <li>10 обращений</li>
                                <li>Кредитный рейтинг</li>
                            </ul>
                        </div>
                        <a href="#order:Расширенный =1490" onclick="
                        setTimeout(function () {
                            $('.t706__cartwin').addClass('t706__cartwin_showed');
                             $('.type_2').attr('href', '#order')
                             $('.type_2').find('.text-to-cart').text('Уже в корзине')
                        }, 500)" target="" class="t599__btn t-btn t-btn_sm type_2"
                           style="color:#ffffff;background-color:#13ce66;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                            <table style="width:100%; height:100%;">
                                <tr>
                                    <td class="text-to-cart">Купить</td>
                                </tr>
                            </table>
                        </a>
                    </div>
                </div>
                <div class="t599__col t-col t-col_4 t-align_center t-animate" data-animate-style="zoomin"
                     data-animate-chain="yes">
                    <div class="t599__content" style="border: 1px solid #e0e6ed; border-radius: 9px;">
                        <div class="t599__title t-name t-name_lg" field="title3" style="">ПРЕМИУМ
                            <br/>
                        </div>
                        <div class="t599__subtitle t-descr t-descr_xxs" field="subtitle3" style="">Количество
                            пользователей:
                            <br/>не ограничено
                        </div>
                        <div class="t599__price t-title t-title_xs" field="price3" style="">₽2490</div>
                        <div class="t599__descr t-descr t-descr_xs" field="descr3" style="">
                            <ul>
                                <li>Пакет на год</li>
                                <li> 12 обращений</li>
                                <li>Оформление налогового вычета</li>
                                <li>Персональное финансовое планирование</li>
                                <li>Кредитная история</li>
                                <li>Кредитный рейтинг</li>
                            </ul>
                        </div>
                        <a href="#order:Премиум =2490" onclick="
                        setTimeout(function () {
                            $('.t706__cartwin').addClass('t706__cartwin_showed');
                             $('.type_3').attr('href', '#order')
                             $('.type_3').find('.text-to-cart').text('Уже в корзине')
                        }, 500)" target="" class="t599__btn t-btn t-btn_sm type_3"
                           style="color:#ffffff;background-color:#13ce66;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                            <table style="width:100%; height:100%;">
                                <tr>
                                    <td class="text-to-cart">Купить</td>
                                </tr>
                            </table>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            #rec61291558 .t599__featured .t599__content {
                box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.10) !important;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    t599_init('61291558');
                }, 500);
                $(window).bind('resize', t_throttle(function () {
                    t599_init('61291558')
                }, 250));
                $('.t599').bind('displayChanged', function () {
                    t599_init('61291558');
                });
            });
            $(window).load(function () {
                t599_init('61291558');
            });
        </script>
    </div>
    <div id="rec65041147" class="r t-rec t-rec_pt_60 t-rec_pb_45" style="padding-top:60px;padding-bottom:45px; "
         data-record-type="61">
        <!-- T051 -->
        <div class="t051">
            <div class="t-container">
                <div class="t-col t-col_12 ">
                    <div class="t051__text t-text t-text_md" style="" field="text">В каждый пакет включены ежемесячные
                        практические финансовые советы по email
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec61256124" class="r t-rec t-rec_pt_150 t-rec_pb_150"
         style="padding-top:150px;padding-bottom:150px;background-color:#eff4f7; " data-record-type="526"
         data-bg-color="#eff4f7">
        <!-- t526 -->
        <div class="t526">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs" field="btitle">Наши ведущие эксперты</div>
                        <div class="t-section__descr t-descr t-descr_xl t-margin_auto" field="bdescr">Для вас работают
                            лучшие люди в мире финансов
                        </div>
                    </div>
                </div>
            </div>
            <div class="t526__container t-container">
                <div class="t526__col t-col t-col_4 t-align_center t526__col-mobstyle">
                    <div class="t526__itemwrapper t526__itemwrapper_3">
                        <div class="t526__imgwrapper t-margin_auto">
                            <div class="t526__bgimg t526__img_circle t-margin_auto t-bgimg"
                                 bgimgfield="li_img__1478015636342"
                                 data-original="{{ asset('public/frontend/images/tild6533-3037-4837-a561-326538626231__1.jpg') }}"
                                 style="    background-size: contain;
    background-color: #535257;
    background-position: center 5px;background-image: url('{{ asset('public/frontend/images/tild6533-3037-4837-a561-326538626231__-__resize__20x__1.jpg') }}"></div>
                        </div>
                        <div class="t526__wrappercenter">
                            <div class="t526__persname t-name t-name_lg t526__bottommargin_sm" style=""
                                 field="li_persname__1478015636342">Дмитрий Чечулин
                                <br/>
                            </div>
                            <div class="t526__persdescr t-descr t-descr_xxs " style=""
                                 field="li_persdescr__1478015636342">Ведущий аналитик
                                <br/>финансовой группы MFC Group
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t526__col t-col t-col_4 t-align_center t526__col-mobstyle">
                    <div class="t526__itemwrapper t526__itemwrapper_3">
                        <div class="t526__imgwrapper t-margin_auto">
                            <div class="t526__bgimg t526__img_circle t-margin_auto t-bgimg"
                                 bgimgfield="li_img__1478015651388"
                                 data-original="{{ asset('public/frontend/images/tild3636-3763-4265-b837-623837353230__2-2.jpg') }}"
                                 style="    background-size: contain;
                                         background-color: #535257;
                                         background-position: center 5px;background-image: url('{{ asset('public/frontend/images/tild3636-3763-4265-b837-623837353230__-__resize__20x__2-2.jpg') }}');"></div>
                        </div>
                        <div class="t526__wrappercenter">
                            <div class="t526__persname t-name t-name_lg t526__bottommargin_sm" style=""
                                 field="li_persname__1478015651388">Антон Ананьев
                                <br/>
                            </div>
                            <div class="t526__persdescr t-descr t-descr_xxs " style=""
                                 field="li_persdescr__1478015651388">Финансовый директор
                                <br/>крупного нефтетрейдерского холдинга
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t526__col t-col t-col_4 t-align_center t526__col-mobstyle">
                    <div class="t526__itemwrapper t526__itemwrapper_3">
                        <div class="t526__imgwrapper t-margin_auto">
                            <div class="t526__bgimg t526__img_circle t-margin_auto t-bgimg"
                                 bgimgfield="li_img__1478015661335"
                                 data-original="{{ asset('public/frontend/images/photo_2019-03-01_11-28-39 copy.jpg') }}"
                                 style=" background-position: 47% 21px;   background-size: contain;
                                         background-color: #535257;
                                         background-image: url('{{ asset('public/frontend/images/photo_2019-03-01_11-28-39 copy.jpg') }}');"></div>
                        </div>
                        <div class="t526__wrappercenter">
                            <div class="t526__persname t-name t-name_lg t526__bottommargin_sm" style=""
                                 field="li_persname__1478015661335">Михаил Теплов
                                <br/>
                            </div>
                            <div class="t526__persdescr t-descr t-descr_xxs " style=""
                                 field="li_persdescr__1478015661335">Ведущий эксперт
                                <br/>в области финансов
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec68113111" class="r t-rec t-rec_pt_105 t-rec_pb_105" style="padding-top:105px;padding-bottom:105px; "
         data-animationappear="off" data-record-type="678">
        <!-- t678 -->
        <div class="t678 ">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs" field="btitle">Форма обратной связи</div>
                        <div class="t-section__descr t-descr t-descr_xl" field="bdescr">Пожалуйста оставьте заявку если
                            вас что-то заинтересовало или остались вопросы
                        </div>
                    </div>
                </div>
            </div>
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div>
                        <form id="form68113111" name='form68113111' role="form" action='' method='POST'
                              data-formactiontype="2" data-inputbox=".t-input-group"
                              class="t-form js-form-proccess t-form_inputs-total_4 "
                              data-success-callback="t678_onSuccess">
                            <input type="hidden" name="formservices[]" value="4a912a8dd40941278024fef31fa4a868"
                                   class="js-formaction-services">
                            <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;">
                                Спасибо! Ваше сообщение успешно отправлено. Мы скоро вам ответим!
                            </div>
                            <div class="t-form__inputsbox">
                                <div class="t-input-group t-input-group_em" data-input-lid="1493283059688">
                                    <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no"
                                         field="li_title__1493283059688" style="">E-mail
                                    </div>
                                    <div class="t-input-block">
                                        <input type="text" name="Email" class="t-input js-tilda-rule " value=""
                                               placeholder="Ваш E-mail" data-tilda-req="1" data-tilda-rule="email"
                                               style="color:#000000; border:1px solid #000000; ">
                                        <div class="t-input-error"></div>
                                    </div>
                                </div>
                                <div class="t-input-group t-input-group_nm" data-input-lid="1494858943227">
                                    <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no"
                                         field="li_title__1494858943227" style="">Имя
                                    </div>
                                    <div class="t-input-block">
                                        <input type="text" name="Name" class="t-input js-tilda-rule " value=""
                                               placeholder="Ваше Имя" data-tilda-req="1" data-tilda-rule="name"
                                               style="color:#000000; border:1px solid #000000; ">
                                        <div class="t-input-error"></div>
                                    </div>
                                </div>
                                <div class="t-input-group t-input-group_ph" data-input-lid="1495040492013">
                                    <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no"
                                         field="li_title__1495040492013" style="">Номер Телефона
                                    </div>
                                    <div class="t-input-block">
                                        <input type="text" name="Phone" class="t-input js-tilda-rule js-tilda-mask "
                                               value="" placeholder="Ваш Номер Телефона" data-tilda-rule="phone"
                                               data-tilda-mask="+7 (999) 999-9999"
                                               style="color:#000000; border:1px solid #000000; ">
                                        <div class="t-input-error"></div>
                                    </div>
                                </div>
                                <div class="t-input-group t-input-group_ta" data-input-lid="1495027254430">
                                    <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no"
                                         field="li_title__1495027254430" style="">Текст Сообщения
                                    </div>
                                    <div class="t-input-block">
                                        <textarea name="Текст Сообщения" class="t-input js-tilda-rule "
                                                  style="color:#000000; border:1px solid #000000; height:204px"
                                                  rows="6"></textarea>
                                        <div class="t-input-error"></div>
                                    </div>
                                </div>
                                <div class="t-form__errorbox-middle">
                                    <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                        <div class="t-form__errorbox-text t-text t-text_md">
                                            <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                            <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                            <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                            <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                            <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                            <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t-form__submit">
                                    <button type="submit" class="t-submit"
                                            style="color:#ffffff;background-color:#13ce66;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                                        Отправить
                                    </button>
                                </div>
                            </div>
                            <div class="t-form__errorbox-bottom">
                                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                    <div class="t-form__errorbox-text t-text t-text_md">
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <style>
                            #rec68113111 input::-webkit-input-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }

                            #rec68113111 input::-moz-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }

                            #rec68113111 input:-moz-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }

                            #rec68113111 input:-ms-input-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }

                            #rec68113111 textarea::-webkit-input-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }

                            #rec68113111 textarea::-moz-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }

                            #rec68113111 textarea:-moz-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }

                            #rec68113111 textarea:-ms-input-placeholder {
                                color: #000000;
                                opacity: 0.5;
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec61256124" class="r t-rec t-rec_pt_150 t-rec_pb_150"
         style="padding-top:150px;padding-bottom:150px;background-color:#eff4f7; " data-record-type="526"
         data-bg-color="#eff4f7">
        <!-- t526 -->
        <div class="t526">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs" field="btitle">Правила оказания услуг</div>
                    </div>
                </div>
            </div>
            <div class="t526__container t-container">
                <div class="t526__col t-col t-align_center t526__col-mobstyle">
                    <div class="t526__itemwrapper t526__itemwrapper_3">
                        <div class="t526__imgwrapper t-margin_auto" style="width: 650px">
                            <div class="t526__bgimg t526__img_circle t-margin_auto t-bgimg"
                                 bgimgfield="li_img__1478015651388"
                                 data-original="{{ asset('public/frontend/images/Rosgosstrah_logo.png') }}"
                                 style="border-radius: 0;
                                         background-size: contain;
                                         padding-bottom: 0;
                                         height: 240px;background-image: url('{{ asset('public/frontend/images/Rosgosstrah_logo.png') }}');"></div>
                        </div>
                        <div class="t526__wrappercenter">
                            <div class="t526__persdescr t-descr t-descr_xxs " style=""
                                 field="li_persdescr__1478015651388">
                                <a target="_blank" href="{{ asset('public/RGS-Finsovet.pdf') }}">Правила оказания услуг</a>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection