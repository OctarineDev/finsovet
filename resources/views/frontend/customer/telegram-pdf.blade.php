<!-- pdf.blade.php -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>

<style type="text/css">

    body {
        font-size: 12px;
        font-family: georgia;
        font-weight: normal;
    }

    table {
        font-family: georgia;
        font-weight: normal;
        border-collapse: collapse;
        width: 100%;
    }

    .head td {
        border: 1px solid #a5a5a5;
        font-weight: bold;
        text-align: right;
        padding: 0 5px;
    }

    .head .td-img {
        text-align: left;
        padding: 0 5px 0 50px;
    }

    .main {
        margin: 30px 0 80px;
    }

    .heading {
        font-size: 24px;
        margin-bottom: 30px;
    }

    ul {
        margin: 0;
        padding: 0;
    }

    .info li {
        display: block;
        font-weight: bold;
        margin-bottom: 15px;
    }

    .red {
        color: #ff0000;
    }

    .center {
        text-align: center;
    }

    .bold {
        font-weight: bold;
    }

    .main-table p {
        margin: 0;
        margin-top: 10px;
    }

    .main-table thead {
        text-align: center;
    }

    .main-table thead td {
        background-color: #c1c1c1;
        vertical-align: top;
    }

    .main-table td {
        border: 1px solid #000000;
        padding: 0 5px;
    }

    .text-help {
        margin-top: 30px;
    }

    .text-help p {
        margin: 0;
    }

    .help-list {
        padding-left: 12px;
        margin-bottom: 15px;
    }

    @font-face {
        font-family: 'georgia';
        font-weight: normal;
        src: url( storage_path('fonts/georgia.ttf')  );
    }

    @font-face {
        font-family: 'georgia';
        font-weight: bold;
        src: url( storage_path('fonts/georgiabold.ttf')  );
    }

</style>

<!-- head -->
{{--<div class="head">--}}
    {{--<table>--}}
        {{--<tr>--}}
            {{--<td>8 (804) 333 0503</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td>finsovet.online</td>--}}
        {{--</tr>--}}
    {{--</table>--}}
{{--</div>--}}

<div class="main">
    {{--<p class="heading center bold">Сертификат</p>--}}
    <ul class="info">
        <li>
            <span class="info-title">Откуда: </span>
        </li>
        <li>
            <span class="info-title">Куда: </span> <br>
            <span>ООО "СВЕЖИЙ СОВЕТ"  109428 г.Москва, Рязанский проспект д.10, кор.18, оф.12</span>
        </li>
        <li>
            <span class="info-title">Текст телеграммы: </span> <br>
            <span>прошу предоставить доступ к сервису кредитная история онлайн  </span>
        </li>
    </ul>
    <div class="text-help">
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000;">{{ $order->surname }} {{ $order->name }} {{ $order->second_name }},  {{ date('d-m-Y', strtotime($order->birthday)) }}, {{ $order->place_of_birth }}</td>
            </tr>
        </table>
        <p style="font-style: italic">ФИО, дата и место рождения,</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000;">{{ $order->identity_document_series }}, {{ $order->id_document_number }}, {{ date('d-m-Y', strtotime($order->identity_date_document)) }}, {{ $order->name_of_issuing_authority }}, </td>
            </tr>
        </table>
        <p style="font-style: italic">паспортные данные (серия, номер, дата и место выдачи),</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000;">{{ $order->phone }}</td>
            </tr>
        </table>
        <p style="font-style: italic">телефон,</p> <br>
        <table>
            <tr>
                <td style="width: 240px;">
                    Собственноручную подпись удостоверяю
                </td>
                <td>
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <p>* Ваша собственноручная подпись должна быть обязательно заверена оператором почтовой связи</p>
    </div>
    {{--<div class="text-help">--}}
        {{--<p class="bold">Как воспользоваться сертификатом:</p>--}}
        {{--<p>Ваш сертификат активируется сразу после оплаты. После <br> активации Вам в течение одного календарного года доступны все услуги, <br> включенные в выбранный тарифный план. Если у вас возник вопрос, <br> разобраться в котором могут помочь специалисты службы «Финансовый <br> советник», Вы можете:</p>--}}
        {{--<ul class="help-list">--}}
            {{--<li>Позвонить нам по многоканальному номеру: 8 (804) 333 0503</li>--}}
        {{--</ul>--}}
        {{--<p>С публичной офертой, а также с правилами оказания услуг Вы можете <br> ознакомиться на сайте finsovet.online</p>--}}
    {{--</div>--}}
</div>
</body>
</html>