<!-- pdf.blade.php -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>

<style type="text/css">

    body {
        font-size: 12px;
        font-family: georgia;
        font-weight: normal;
    }

    table {
        font-family: georgia;
        font-weight: normal;
        border-collapse: collapse;
        width: 100%;
    }

    .head td {
        border: 1px solid #a5a5a5;
        font-weight: bold;
        text-align: right;
        padding: 0 5px;
    }

    .head .td-img {
        text-align: left;
        padding: 0 5px 0 50px;
    }

    .main {
        margin-top: 10px;
    }

    .heading {
        font-size: 18px;
        margin-bottom: 30px;
    }

    ul {
        margin: 0;
        padding: 0;
    }

    .info li {
        display: block;
        font-weight: bold;
        margin-bottom: 15px;
    }

    .red {
        color: #ff0000;
    }

    .center {
        text-align: center;
    }

    .bold {
        font-weight: bold;
    }

    .main-table p {
        margin: 0;
        margin-top: 10px;
    }

    .main-table thead {
        text-align: center;
    }

    .main-table thead td {
        background-color: #c1c1c1;
        vertical-align: top;
    }

    .main-table td {
        border: 1px solid #000000;
        padding: 0 5px;
    }

    .text-help {
        margin-top: 30px;
    }

    .text-help p {
        margin: 0;
    }

    .help-list {
        padding-left: 12px;
        margin-bottom: 15px;
    }

    @font-face {
        font-family: 'georgia';
        font-weight: normal;
        src: url(storage_path('fonts/georgia.ttf')  );
    }

    @font-face {
        font-family: 'georgia';
        font-weight: bold;
        src: url(storage_path('fonts/georgiabold.ttf')  );
    }

</style>

<!-- head -->
<p style="text-align: right;">
    В Национальное бюро кредитных историй <br>
    С пометкой «Запрос на получение кредитного отчета»
</p>
<div class="main">
    <p class="heading center bold">Запрос Субъекта кредитной истории на получение кредитного отчета по своейкредитной
        истории</p>
    <div class="text-help">
        <table>
            <tr>
                <td style="width: 20px;">Я,</td>
                <td style="border-bottom: 1px solid #000; text-align: center"><span
                            style="margin-left: -20px">{{ $order->surname }}</span></td>
            </tr>
        </table>
        <p style="font-style: italic; text-align: center">(фамилия)</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center">{{ $order->name }}</td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(имя)</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center">{{ $order->second_name }}</td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(отчество)</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center">{{ date('d-m-Y', strtotime($order->birthday)) }}</td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(дата рождения)</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center">{{ $order->place_of_birth }}</td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(место рождения)</p> <br>
        <p>На основании документа, удостоверяющего личность: Паспорт</p> <br>
        <table>
            <tr>
                <td style="width: 100px;">
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;">{{ $order->identity_document_series }}</td>
                        </tr>
                    </table>
                    <p style="font-style: italic">(серия)</p>
                </td>
                <td>
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;">{{ $order->id_document_number }}</td>
                        </tr>
                    </table>
                    <p style="font-style: italic">(номер)</p>
                </td>
            </tr>
        </table>
        <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center">{{ $order->name_of_issuing_authority }} {{ date('d-m-Y', strtotime($order->identity_date_document)) }}</td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(кем и когда выдан)</p> <br>
        <p>и дополнительных данных:</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center">{{ $order->tin }}</td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(идентификационный номер налогоплательщика)</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center"></td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(страховой номер обязательного пенсионного страхования)</p>
        <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center">{{ $order->address_register }} {{ $order->street_register }}</td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(адрес места жительства (регистрации))</p> <br>
        <table>
            <tr>
                <td style="border-bottom: 1px solid #000; text-align: center"></td>
            </tr>
        </table>
        <p style="font-style: italic;text-align: center">(адрес для направления корреспонденции)</p> <br>
        <p>
            Настоящим поручаю <span
                    style="border-bottom: 1px solid #000; display: inline-block; vertical-align: middle; padding-top: 6px; padding-bottom: 1px; line-height: 10px">OOO "СВЕЖИЙ СОВЕТ"</span>
            <span style="border-bottom: 1px solid #000; display: inline-block; vertical-align: middle; padding-top: 6px; padding-bottom: 1px; line-height: 10px">ИНН:7733267801 ОГРН:1167746104030</span>
            запросить в АО «НБКИ», получить и выдать мне кредитный отчет по моей кредитной истории всоответствии с
            Федеральным законом «О кредитных историях»
            <br>
            Настоящим даю свое согласие <span
                    style="border-bottom: 1px solid #000; display: inline-block; vertical-align: middle; padding-top: 6px; padding-bottom: 1px; line-height: 10px">OOO "СВЕЖИЙ СОВЕТ"</span>
            <span style="border-bottom: 1px solid #000; display: inline-block; vertical-align: middle; padding-top: 6px; padding-bottom: 1px; line-height: 10px">ИНН:7733267801 ОГРН:1167746104030</span>,
            а также АО «НБКИ» на обработку персональных данных, указанных выше, следующими способами:сбор (получение),
            запись, систематизация, использование, уточнение (обновление, изменение),хранение, передача, блокирование и
            уничтожение, как с применением средств автоматизации, таки без их использования, а также данных,
            содержащихся в кредитном отчете по моей кредитнойистории, исключительно способами: получение, передача и
            уничтожение как с применениемсредств автоматизации, так и без их использования. <br>
            Обработка персональных данных осуществляется исключительно c целью осуществления запросав АО «НБКИ», его
            обработки и выдачи мне кредитного отчета по моей кредитной истории. Согласие на обработку персональных
            данных дается мной своей волей и в своем интересе.Согласие действует в течение <span
                    style="border-bottom: 1px solid #000; display: inline-block; vertical-align: middle; padding-top: 6px; padding-bottom: 1px; line-height: 10px">30 дней</span>
            с момента предоставления и может быть отозвано.
        </p> <br>
        <p><span style="display: inline-block; width: 10px;height: 10px;border: 1px solid #000;"></span> Согласен</p> <br>
        <p><span style="display: inline-block; width: 10px;height: 10px;border: 1px solid #000;"></span> Я даю свое согласие на обработку персональных данных, осуществляемую безиспользования средств автоматизации.</p>
        <br>
        <table>
            <tr>
                <td style="width: 150px;">
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;"></td>
                        </tr>
                    </table>
                    <p style="font-style: italic">Дата</p>
                </td>
                <td style="width: 150px;">
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;"></td>
                        </tr>
                    </table>
                    <p style="font-style: italic">Подпись</p>
                </td>
                <td>
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;"></td>
                        </tr>
                    </table>
                    <p style="font-style: italic">Расшифровка подписи</p>
                </td>
            </tr>
        </table>
        <br>
        <p>Кредитный отчет мною получен.</p><br>
        <table>
            <tr>
                <td style="width: 150px;">
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;"></td>
                        </tr>
                    </table>
                    <p style="font-style: italic">Дата</p>
                </td>
                <td style="width: 150px;">
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;"></td>
                        </tr>
                    </table>
                    <p style="font-style: italic">Подпись</p>
                </td>
                <td>
                    <table>
                        <tr>
                            <td style="border-bottom: 1px solid #000;"></td>
                        </tr>
                    </table>
                    <p style="font-style: italic">Расшифровка подписи</p>
                </td>
            </tr>
        </table>
    </div>
    {{--<div class="text-help">--}}
    {{--<p class="bold">Как воспользоваться сертификатом:</p>--}}
    {{--<p>Ваш сертификат активируется сразу после оплаты. После <br> активации Вам в течение одного календарного года доступны все услуги, <br> включенные в выбранный тарифный план. Если у вас возник вопрос, <br> разобраться в котором могут помочь специалисты службы «Финансовый <br> советник», Вы можете:</p>--}}
    {{--<ul class="help-list">--}}
    {{--<li>Позвонить нам по многоканальному номеру: 8 (804) 333 0503</li>--}}
    {{--</ul>--}}
    {{--<p>С публичной офертой, а также с правилами оказания услуг Вы можете <br> ознакомиться на сайте finsovet.online</p>--}}
    {{--</div>--}}
</div>
</body>
</html>