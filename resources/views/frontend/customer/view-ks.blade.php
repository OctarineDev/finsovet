@extends('frontend.includes.header')
@section('content')
    <div class="r t-rec custom-section" style="padding-top:150px;padding-bottom:300px;font-family: 'Roboto',Arial,sans-serif;"
         data-animationappear="off" data-record-type="18">
        <div class="t-container">
            <div class="t-col t-col_12 ">
                <div class="t-section__title t-title t-title_xs" style="text-align: center" field="btitle">
                    Данные о кредитном рейтинге
                </div>
                {{--<div class="chart">--}}
                {{--<div class="chart__column">--}}
                {{--<div class="chart__text"><span>0-400</span><br>низкий<br></div>--}}
                {{--</div>--}}
                {{--<div class="chart__column">--}}
                {{--<div class="chart__text"><span>400-600</span><br>низкий</div>--}}
                {{--</div>--}}
                {{--<div class="chart__column">--}}
                {{--<div class="chart__text"><span>600-700</span><br>удовлетворительный</div>--}}
                {{--</div>--}}
                {{--<div class="chart__column">--}}
                {{--<div class="chart__text"><span>700-760</span><br>достаточно высокий</div>--}}
                {{--</div>--}}
                {{--<div class="chart__column">--}}
                {{--<div class="chart__text"><span>760-790</span><br>высокий</div>--}}
                {{--</div>--}}
                {{--<div class="chart__column">--}}
                {{--<div class="chart__text"><span>799-10000</span><br>высокий</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div id="chart-1" class="chart-container" data-chart="{{$score}}"></div>
                <?php $text = 'Советуем внимательно ознакомиться с Памяткой для повышения кредитного здоровья.'; ?>
                @switch($score)
                    @case($score <= 400)
                    <?php $mark = 'низком';  ?>
                    @break
                    @case($score <= 600)
                    <?php $mark = 'низком';  ?>
                    @break
                    @case($score <= 700)
                    <?php $mark = 'удовлетворительном';  ?>
                    @break
                    @case($score <= 760)
                    <?php $mark = 'достаточно высоком';  ?>
                    @break
                    @case($score <= 790)
                    <?php
                        $mark = 'высоком';
                        $text = '';
                    ?>
                    @break
                    @case($score >= 791)
                    <?php
                        $mark = 'высоком';
                        $text = '';
                    ?>
                    @break

                @endswitch
                <p style="max-width: 1000px;margin: 0 auto 0;text-align: center;">
                    Ваш кредитный рейтинг находится на <b>{{ $mark }}</b> уровне.<br>
                    {{ $text }}
                </p>
                <p style="max-width: 1000px;margin: 0 auto 0;text-align: center;"><br>
                    Кредитный рейтинг – это показатель Вашей кредитоспособности и финансовой репутации.
                    Кредитный рейтинг заемщика помогает кредитным организациям в принятии решения о предоставлении Вам займа и, по сути, является основным критерием при оценке риска невозврата займа.<br>
                    Кредитный рейтинг также будет интересен страховым компаниям, нотариусам и работодателям, которые смогут проверять сотрудников при найме на ответственные позиции.
                    Чем выше ваш кредитный рейтинг, тем выше шанс в одобрении кредита.<br>
                    В случае, если Ваш кредитный рейтинг находится на низком или удовлетворительном уровне, советуем внимательно ознакомиться с Памяткой Кредитного Здоровья с целью снижения кредитного риска.
                </p>
                <div class="reg-page-form__char search-form__char reg-page-form__char_flex"
                     style="text-align: center; width: 100%;">
                    <a href="/customer/profile"
                       class="t-submit"
                       style="display: inline-block;line-height: 60px;color:#ffffff;background-color:#13ce66;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;margin-top: 30px;">
                        Вернуться в ЛК
                    </a>
                </div>
            </div>
        </div>
        <div id="rec61290678" class="r t-rec" style=" " data-animationappear="off" data-record-type="651">
            <!-- T651 -->
            <div class="t651" style="">
                <div class="t651__btn" style="">
                    <div class="t651__btn_wrapper " style="background:#75d17a;">
                        <svg class="t651__icon" style="fill:#ffffff;" xmlns="http://www.w3.org/2000/svg" width="30px"
                             height="30px" viewBox="0 0 19.3 20.1">
                            <desc>Call</desc>
                            <path d="M4.6 7.6l-.5-.9 2-1.2L4.6 3l-2 1.3-.6-.9 2.9-1.7 2.6 4.1"/>
                            <path d="M9.9 20.1c-.9 0-1.9-.3-2.9-.9-1.7-1-3.4-2.7-4.7-4.8-3-4.7-3.1-9.2-.3-11l.5.9C.2 5.7.4 9.7 3 13.9c1.2 2 2.8 3.6 4.3 4.5 1.1.6 2.7 1.1 4.1.3l1.9-1.2L12 15l-2 1.2c-1.2.7-2.8.3-3.5-.8l-3.2-5.2c-.7-1.2-.4-2.7.8-3.5l.5.9c-.7.4-.9 1.3-.5 2l3.2 5.2c.4.7 1.5.9 2.2.5l2.8-1.7 2.6 4.1-2.8 1.7c-.7.5-1.4.7-2.2.7zM13.7 11.3l-.9-.3c.4-1.1.2-2.2-.4-3.1-.6-1-1.7-1.6-2.8-1.7l.1-1c1.5.1 2.8.9 3.6 2.1.7 1.2.9 2.7.4 4z"/>
                            <path d="M16.5 11.9l-1-.3c.5-1.8.2-3.7-.8-5.3-1-1.6-2.7-2.6-4.7-2.9l.1-1c2.2.3 4.2 1.5 5.4 3.3 1.2 1.9 1.6 4.1 1 6.2z"/>
                            <path d="M18.9 12.5l-1-.3c.7-2.5.2-5.1-1.1-7.2-1.4-2.2-3.7-3.6-6.3-4l.1-1c2.9.4 5.4 2 7 4.4 1.6 2.4 2.1 5.3 1.3 8.1z"/>
                        </svg>
                        <svg class="t651__icon-close" width="16px" height="16px" viewBox="0 0 23 23" version="1.1"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <desc>Close</desc>
                            <g stroke="none" stroke-width="1" fill="#000" fill-rule="evenodd">
                                <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                                <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="t651__popup">
                    <div class="t651__popup-container" style="">
                        <div class="t651__wrapper">
                            <div class="t651__title t-name t-name_xl" style="">Введите свой контактный телефон, и мы
                                свяжемся с вами в ближайшее время!
                            </div>
                            <form id="form61290678" name='form61290678' role="form"
                                  action='https://forms.tildacdn.com/procces/' method='POST' data-formactiontype="2"
                                  data-inputbox=".t651__blockinput" class="js-form-proccess " data-tilda-captchakey="">
                                <input type="hidden" name="formservices[]" value="4a912a8dd40941278024fef31fa4a868"
                                       class="js-formaction-services">
                                <div class="t651__input-container">
                                    <div class="t651__allert-wrapper">
                                        <div class="js-errorbox-all t651__blockinput-errorbox" style="display:none;">
                                            <div class="t651__blockinput-errors-text t-descr t-descr_xs">
                                                <p class="t651__blockinput-errors-item js-rule-error js-rule-error-all"></p>
                                                <p class="t651__blockinput-errors-item js-rule-error js-rule-error-req">
                                                    Required field</p>
                                                <p class="t651__blockinput-errors-item js-rule-error js-rule-error-email">
                                                    Please correct e-mail address</p>
                                                <p class="t651__blockinput-errors-item js-rule-error js-rule-error-name">
                                                    Name Wrong. Correct please</p>
                                                <p class="t651__blockinput-errors-item js-rule-error js-rule-error-phone">
                                                    Please correct phone number</p>
                                                <p class="t651__blockinput-errors-item js-rule-error js-rule-error-string">
                                                    Please enter letter, number or punctuation symbols.</p>
                                            </div>
                                        </div>
                                        <div class="js-successbox t651__blockinput-success t-text t-text_md"
                                             style="display:none;">
                                            <div class="t651__success-icon">
                                                <svg width="50px" height="50px" viewBox="0 0 50 50">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g fill="#222">
                                                            <path d="M25.0982353,49.2829412 C11.5294118,49.2829412 0.490588235,38.2435294 0.490588235,24.6752941 C0.490588235,11.1064706 11.53,0.0670588235 25.0982353,0.0670588235 C38.6664706,0.0670588235 49.7058824,11.1064706 49.7058824,24.6752941 C49.7058824,38.2441176 38.6664706,49.2829412 25.0982353,49.2829412 L25.0982353,49.2829412 Z M25.0982353,1.83176471 C12.5023529,1.83176471 2.25529412,12.0794118 2.25529412,24.6752941 C2.25529412,37.2705882 12.5023529,47.5182353 25.0982353,47.5182353 C37.6941176,47.5182353 47.9411765,37.2705882 47.9411765,24.6752941 C47.9411765,12.0794118 37.6941176,1.83176471 25.0982353,1.83176471 L25.0982353,1.83176471 Z"></path>
                                                            <path d="M22.8435294,30.5305882 L18.3958824,26.0829412 C18.0511765,25.7382353 18.0511765,25.18 18.3958824,24.8352941 C18.7405882,24.4905882 19.2988235,24.4905882 19.6435294,24.8352941 L22.8429412,28.0347059 L31.7282353,19.1488235 C32.0729412,18.8041176 32.6311765,18.8041176 32.9758824,19.1488235 C33.3205882,19.4935294 33.3205882,20.0517647 32.9758824,20.3964706 L22.8435294,30.5305882 L22.8435294,30.5305882 Z"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="t651__success-message t-descr t-descr_sm">Cпасибо! Мы свяжемся с
                                                вами в ближайшее время.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="t651__input-wrapper">
                                        <div class="t651__blockinput">
                                            <input type="text" name="phone" class="t651__input t-input js-tilda-rule "
                                                   value="" placeholder="+7 (000) 000 0000" data-tilda-req="1"
                                                   data-tilda-rule="phone" style=" border:1px solid #d1d1d1; "></div>
                                        <div class="t651__blockbutton">
                                            <button type="submit" class="t651__submit t-submit"
                                                    style="background-color:#199c68;text-transform:uppercase;">Позвоните
                                                мне
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="t651__additional-info">
                                <div class="t651__text t-descr t-descr_sm" style="">Или Вы можете позвонить нам сами.
                                </div>
                                <div class="t651__phone t-descr t-descr_sm" style="">8 (804) 333 05 03</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    t651_initPopup('61290678');
                }, 500);

                var gaugeOptions = {
                    chart: {
                        type: 'solidgauge'
                    },
                    title: null,
                    pane: {
                        center: ['50%', '85%'],
                        size: '140%',
                        startAngle: -90,
                        endAngle: 90,
                        background: {
                            backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
                            innerRadius: '60%',
                            outerRadius: '100%',
                            shape: 'arc'
                        }
                    },
                    tooltip: {
                        enabled: false
                    },
                    // the value axis
                    yAxis: {
                        stops: [
                            [0.16, '#b9272a'],
                            [0.32, '#c64b30'],
                            [0.48, '#c7703b'],
                            [0.64, '#d6d054'],
                            [0.8, '#a1d25c'],
                            [0.96, '#13ce66'],
                        ],
                        lineWidth: 0,
                        minorTickInterval: null,
                        tickAmount: 2,
                        title: {
                            y: -70
                        },
                        labels: {
                            y: 16
                        }
                    },
                    plotOptions: {
                        solidgauge: {
                            dataLabels: {
                                y: 5,
                                borderWidth: 0,
                                useHTML: true
                            }
                        }
                    }
                };

                var chartValue = Number($('#chart-1').attr('data-chart'));
                // The speed gauge
                var chartSpeed = Highcharts.chart('chart-1', Highcharts.merge(gaugeOptions, {
                    yAxis: {
                        min: 0,
                        max: 1000,
                        title: {
                            text: ''
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Speed',
                        data: [chartValue],
                        dataLabels: {
                            format:
                            '<div style="text-align:center;margin-top: -82px;">' +
                            '<span style="font-size:25px">{y}</span><br/>' +
                            '</div>'
                        },
                        tooltip: {
                            valueSuffix: ' '
                        }
                    }]
                }));
            });
        </script>
    </div>
@endsection