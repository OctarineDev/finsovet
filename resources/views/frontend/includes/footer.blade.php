<div id="rec61251690" class="r t-rec t-rec_pb_60" style="background-color:#111111; " data-animationappear="off"
     data-record-type="389" data-bg-color="#111111">
    <!-- T389 -->
    <div class="t389" id="t-footer_61251690">
        <div class="t389__maincontainer" style="height: 70px;">
            <div class="t389__content">
                <div class="t389__col t389__col_hiddenmobile">
                    <div class="t389__typo t389__copyright t-name t-name_xs" field="text"
                         style="color: #ffffff;width: 500px;font-size:12px;margin-top: 20px;"><p>ООО "Свежий совет"
                            2016-2018</p>
                        <p>109428 Москва, Рязанский проспект, дом 10,</p>
                        <p> строение 18,
                            3 этаж, комната 12</p>
                        <p> ОГРН: 1167746104030 ИНН: 7733267801</p>
                        <p>ТОЧКА ПАО БАНКА «ФК ОТКРЫТИЕ»</p>
                        <p>БИК: 044525999 РС: 40702810401500021488
                        </p>
                        <p><br><a target="_blank" style="color: #ffffff;" href="/public/Пользовательское_соглашение_Finsovet.docx">Пользовательское соглашение</a>
                        </p>
                        <p><br><a target="_blank" style="color: #ffffff;" href="/public/Условия_использования_сервиса_Finsovet.docx">Условия использования</a>
                        </p>
                    </div>
                </div>
                <div class="t389__col t389__col_center t-align_center">
                    <div class="t389__centercontainer">
                        <ul class="t389__list">
                            <li class="t389__list_item t-name t-name_xs"><a class="t389__typo"
                                                                            style="color: #ffffff;"
                                                                            href="#rec61198875"
                                                                            data-menu-item-number="1">О нас</a></li>
                            </li>

                            </li>
                            <li class="t389__list_item t-name t-name_xs"><a class="t389__typo"
                                                                            style="color: #ffffff;"
                                                                            href="#rec61248049"
                                                                            data-menu-item-number="4">Контакты</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="t389__col t389__col_mobile">
                    <div class="t389__typo t389__copyright t-name t-name_xs" field="text" style="color: #ffffff;">
                        <p>ООО "Свежий совет" 2016-2018</p>
                        <p>109428 Москва, Рязанский проспект, дом 10,</p>
                        <p> строение 18,
                            3 этаж, комната 12</p>
                        <p> ОГРН: 1167746104030 ИНН: 7733267801</p>
                        <p>ТОЧКА ПАО БАНКА «ФК ОТКРЫТИЕ»</p>
                        <p>БИК: 044525999 РС: 40702810401500021488
                        </p>
                        <p><br><a target="_blank" style="color: #ffffff;" href="/public/Пользовательское_соглашение_Finsovet.docx">Пользовательское соглашение</a>
                        </p>
                        <p><br><a target="_blank" style="color: #ffffff;" href="/public/Условия_использования_сервиса_Finsovet.docx">Условия использования</a>
                        </p>
                    </div>
                </div>
                <div class="t389__col">
                    <div class="t389__scroll t-align_right"><a class="t389__typo t-name t-name_xs t389_scrolltop"
                                                               style="color: #ffffff;"
                                                               href="javascript:t389_scrollToTop();"> Наверх <span
                                    class="t389__icon"> <svg width="5px" height="17px" viewBox="0 0 6 20"
                                                             version="1.1"> <defs></defs> <g id="Welcome"
                                                                                             stroke="none"
                                                                                             stroke-width="1"
                                                                                             fill="none"
                                                                                             fill-rule="evenodd"
                                                                                             sketch:type="MSPage"> <g
                                                id="Desktop-HD-Copy-39" sketch:type="MSArtboardGroup"
                                                transform="translate(-569.000000, -1797.000000)" fill="#ffffff"> <path
                                                    d="M565.662286,1804.2076 L562.095536,1806.87166 C561.958036,1807.00916 561.958036,1807.16385 562.095536,1807.30135 L565.662286,1809.96541 C565.799786,1810.10291 565.941411,1810.0431 565.941411,1809.83616 L565.941411,1808.11741 L581.816411,1808.11741 L581.816411,1806.05491 L565.941411,1806.05491 L565.941411,1804.33616 C565.941411,1804.18147 565.866474,1804.1141 565.769536,1804.14297 C565.737224,1804.1526 565.696661,1804.17322 565.662286,1804.2076 Z"
                                                    id="Shape" sketch:type="MSShapeGroup"
                                                    transform="translate(571.904411, 1807.088000) rotate(-270.000000) translate(-571.904411, -1807.088000) "></path> </g> </g></svg> </span>
                        </a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="rec68110921" class="r t-rec" style=" " data-animationappear="off" data-record-type="706">
    <script type="text/javascript">
        $(document).ready(function () {
            tcart__init('68110921');
        });
    </script>
    <div class="t706" data-opencart-onorder="yes" data-project-currency="₽" data-project-currency-side="l"
         data-project-currency-sep="," data-payment-system="yakassa">
        <div class="t706__carticon" style="">
            <div class="t706__carticon-text t-name t-name_xs">Click to order</div>
            <div class="t706__carticon-wrapper">
            <!--<div class="t706__carticon-imgwrap"><img class="t706__carticon-img" src="{{ asset('public/frontend/images/lib__linea__930cac0f-758b-b7ee-1eb0-b18fc6e10893__ecommerce_bag.svg') }}"></div>-->
                <div class="t706__carticon-imgwrap">
                    <svg class="t706__carticon-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                        <descr style="color:#bebebe;">Cart</descr>
                        <path fill="none" stroke-width="2" stroke-miterlimit="10" d="M44 18h10v45H10V18h10z"/>
                        <path fill="none" stroke-width="2" stroke-miterlimit="10"
                              d="M22 24V11c0-5.523 4.477-10 10-10s10 4.477 10 10v13"/>
                    </svg>
                </div>
                <div class="t706__carticon-counter"></div>
            </div>
        </div>
        <div class="t706__cartwin">
            <div class="t706__cartwin-close">
                <div class="t706__cartwin-close-wrapper">
                    <svg class="t706__cartwin-close-icon" width="23px" height="23px" viewBox="0 0 23 23"
                         version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="t706__cartwin-content">
                <div class="t706__cartwin-top">
                    <div class="t706__cartwin-heading t-name t-name_xl">Ваш заказ</div>
                </div>
                <div class="t706__cartwin-products"></div>
                <div class="t706__cartwin-bottom">
                    <div class="t706__cartwin-prodamount-wrap t-descr t-descr_sm"><span
                                class="t706__cartwin-prodamount-label">Total:&nbsp;</span><span
                                class="t706__cartwin-prodamount"></span></div>
                </div>
                <div class="t706__orderform ">
                    <form id="form68110921" name='form68110921' role="form" action='' method='POST'
                          data-formactiontype="2" data-inputbox=".t-input-group"
                          class="t-form js-form-proccess t-form_inputs-total_3 "
                          data-success-callback="t678_onSuccess" data-formsended-callback="t706_onSuccessCallback">
                        <input type="hidden" name="formservices[]" value="65007e8574de659045b1d9adffee9616"
                               class="js-formaction-services">
                        <input type="hidden" name="formservices[]" value="4a912a8dd40941278024fef31fa4a868"
                               class="js-formaction-services">
                        <input type="hidden" name="tildaspec-formname" tabindex="-1" value="Cart">
                        <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;"></div>
                        <div class="t-form__inputsbox">
                            <div class="t-input-group t-input-group_nm" data-input-lid="1496239431201">
                                <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no"
                                     field="li_title__1496239431201" style="">Ваше Имя
                                </div>
                                <div class="t-input-block">
                                    <input type="text" name="Name" class="t-input js-tilda-rule " value=""
                                           placeholder="Фамилия Имя Отчество" data-tilda-req="1"
                                           data-tilda-rule="name" style="color:#000000; border:1px solid #000000; ">
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-input-group t-input-group_em" data-input-lid="1496239459190">
                                <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no"
                                     field="li_title__1496239459190" style="">Ваш E-mail
                                </div>
                                <div class="t-input-block">
                                    <input type="text" name="Email" class="t-input js-tilda-rule " value=""
                                           placeholder="example@site.ru" data-tilda-req="1" data-tilda-rule="email"
                                           style="color:#000000; border:1px solid #000000; ">
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-input-group t-input-group_ph" data-input-lid="1496239478607">
                                <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no"
                                     field="li_title__1496239478607" style="">Ваш Номер
                                </div>
                                <div class="t-input-block">
                                    <input type="text" name="Phone" class="t-input js-tilda-rule js-tilda-mask "
                                           value="" placeholder="+7 (999) 999-9999" data-tilda-req="1"
                                           data-tilda-rule="phone" data-tilda-mask="+7 (999) 999-9999"
                                           style="color:#000000; border:1px solid #000000; ">
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-form__errorbox-middle">
                                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                    <div class="t-form__errorbox-text t-text t-text_md">
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="t-form__submit">
                                <button type="submit" class="t-submit"
                                        style="color:#ffffff;background-color:#13ce66;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                                    Оплатить заказ
                                </button>
                            </div>
                        </div>
                        <div class="t-form__errorbox-bottom">
                            <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                <div class="t-form__errorbox-text t-text t-text_md">
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                                </div>
                            </div>
                        </div>
                    </form>
                    <style>
                        #rec68110921 input::-webkit-input-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }

                        #rec68110921 input::-moz-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }

                        #rec68110921 input:-moz-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }

                        #rec68110921 input:-ms-input-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }

                        #rec68110921 textarea::-webkit-input-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }

                        #rec68110921 textarea::-moz-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }

                        #rec68110921 textarea:-moz-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }

                        #rec68110921 textarea:-ms-input-placeholder {
                            color: #000000;
                            opacity: 0.5;
                        }
                    </style>
                </div>
            </div>
        </div>
        <div class="t706__cartdata"></div>
    </div>
</div>
<div class="popup popup-partner">
    <div class="popup__exit js-close-popup">
        <svg class="t706__cartwin-close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
            </g>
        </svg>
    </div>
    <div class="popup__wrap">
        {{--<div class="title-w">--}}
        {{--<div class="title">--}}
        {{--<p>Партнёрам</p>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="text" style="text-align: center;">
            <p>Для обсуждения вариантов партнерства, просьба связаться с нами по телефону  +7 (985) 686-68-88</p>
        </div>
    </div>
</div>
<div class="popup popup-get">
    <div class="popup__exit js-close-popup">
        <svg class="t706__cartwin-close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
            </g>
        </svg>
    </div>
    <div class="popup__wrap">
		<div class="t706__cartwin-top">
			<div class="t706__cartwin-heading t-name t-name_xl">Заполните форму для получения помощи</div>
		</div>
	    <form class="form-horizontal form-teleport" action="#" style="margin-top:30px">
		    <div class="reg-page-form__char search-form__char" style="margin-bottom:25px;">
				<div class="t-input-title t-descr t-descr_md" style="font-size: 16px;">Ваша Фамилия</div>
				<input required type="text" class="reg-page-form__input" placeholder="Фамилия" name="last_name" value="">
		    </div>
		    <div class="reg-page-form__char search-form__char" style="margin-bottom:25px;">
				<div class="t-input-title t-descr t-descr_md" style="font-size: 16px;">Ваше Имя</div>
				<input required type="text" class="reg-page-form__input" placeholder="Имя" name="first_name" value="">
		    </div>
		    <div class="reg-page-form__char search-form__char" style="margin-bottom:25px;">
				<div class="t-input-title t-descr t-descr_md" style="font-size: 16px;">Ваше Отчество</div>
				<input required type="text" class="reg-page-form__input" placeholder="Отчество" name="middle_name" value="">
		    </div>
		    <div class="reg-page-form__char search-form__char" style="margin-bottom:25px;">
				<div class="t-input-title t-descr t-descr_md" style="font-size: 16px;">Email</div>
				<input required type="text" class="reg-page-form__input" placeholder="Email" name="email" value="">
		    </div>
			<div class="reg-page-form__char search-form__char" style="margin-bottom:25px;">
				<div class="t-input-title t-descr t-descr_md" style="font-size: 16px;">Ваш Номер</div>
				<input type="text" name="phone" class="reg-page-form__input t-input js-tilda-rule js-tilda-mask "
					value="" placeholder="+7 (999) 999-9999" data-tilda-req="1"
					data-tilda-rule="phone" data-tilda-mask="+7 (999) 999-9999" required>
			</div>
			<div class="reg-page-form__char search-form__char" style="margin-bottom:25px;">
				<div class="t-input-title t-descr t-descr_md" style="font-size: 16px;">Ваша Дата рождения</div>
				<input required type="text" class="reg-page-form__input" placeholder="Дата рождения" name="birthday" value="" maxlength="10" autocomplete="off">
		    </div>
			<div class="reg-page-form__char search-form__char" style="margin-bottom:25px;">
				<div class="t-input-title t-descr t-descr_md" style="font-size: 16px;">Ваш город проживания</div>
				<input required type="text" class="reg-page-form__input" placeholder="Город проживания" name="residential_city" value="">
		    </div>
			<div class="reg-page-form__char search-form__char reg-page-form__char_flex" style="text-align: center;">
				<button type="submit" class="t-submit" style="color:#ffffff;background-color: #13ce66;border-radius:30px;-moz-border-radius:30px;-webkit-border-radius:30px;max-width: 100%; font-size: 20px;width: 100%;">
					Получить
				</button>
			</div>
	    </form>
    </div>
</div>
<div class="popup popup-succeed">
    <div class="popup__exit js-close-popup">
        <svg class="t706__cartwin-close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                      x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
            </g>
        </svg>
    </div>
    <div class="popup__wrap">
        <div class="text" style="margin:0;text-align: center;font-size:20px;">
            <p>Спасибо за обращение!</p>
			<p>Инструкции по улучшению Кредитного Здоровья отправлены Вам на почту.</p>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    .t706__carticon_showed {
        display: none;
    }
    .popup {
        opacity: 0;
        background-color: rgba(0, 0, 0, .8);
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        pointer-events: none;
        z-index: -1;
        overflow-y: auto;
        padding: 0 15px;
		display: flex;
		flex-direction: column;
    }

    .popup.active {
        opacity: 1;
        pointer-events: visible;
        z-index: 9999;
    }

    .popup__exit {
        position: fixed;
        top: 30px;
        right: 30px;
        cursor: pointer;
    }

    .popup__wrap {
        margin: auto;
        width: 100%;
        max-width: 560px;
        background-color: #fff;
        padding: 40px;
        box-sizing: border-box;
    }

    .popup__wrap--big {
        max-width: 1000px;
    }

    .title {
        font-family: 'Roboto', Arial, sans-serif;
        font-weight: 600;
        color: #000;
        font-size: 24px;
        line-height: 1.35;
    }

    .text {
        font-family: 'Roboto',Arial,sans-serif;
        font-weight: 300;
        color: #000;
        margin-top: 15px;
    }
	.reg-page-form__input {
		color: #000000;
    	border: 1px solid #000000;
		margin: 0;
		font-family: 'Roboto',Arial,sans-serif;
		font-size: 100%;
		height: 60px;
		padding: 0 20px;
		font-size: 16px;
		line-height: 1.33;
		width: 100%;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		outline: none;
		-webkit-appearance: none;
	}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        // PopUp
        function popUp() {
            $('.js-popup-button').on('click', function (e) {
                e.preventDefault();
                
                $('.popup').removeClass('active');
                var popupClass = '.' + $(this).attr('data-popupShow');

                $(popupClass).addClass('active');
            });

            // Close PopUp
            $('.js-close-popup').on('click', function (e) {
                e.preventDefault();

                $('.popup').removeClass('active');
            });

            $('.popup').on('click', function (e) {
                var div = $(".popup__wrap");

                if (!div.is(e.target) && div.has(e.target).length === 0) {
                    $('.popup').removeClass('active');
                }
            });
        }
        popUp();

		// Input date
		if (Boolean(document.querySelectorAll('input[name="birthday"]')) !== Boolean(null)) {
			var inputsDate = document.querySelectorAll('input[name="birthday"]');

			function dateInputMask(elm) {
				elm.addEventListener('keypress', function(e) {
					if(e.keyCode < 47 || e.keyCode > 57) {
						e.preventDefault();
					}

					var len = elm.value.length;

					if (len !== 1 || len !== 3) {
						if(e.keyCode == 47) {
							e.preventDefault();
						}
					}

					if (len === 2) {
						elm.value += '-';
					}

					if (len === 5) {
						elm.value += '-';
					}
				});
			}

			for (var i = 0; i < inputsDate.length; i++) {
				dateInputMask(inputsDate[i]);
			}
		}

		// Ajax
		(function ajaxFn() {
			var url = 'https://gate.vteleport.ru/sendrequest';

			function sendAjax() {
				var $this = $(this);
				var last_name = $this.find('input[name="last_name"]').val();
				var first_name = $this.find('input[name="first_name"]').val();
				var middle_name = $this.find('input[name="middle_name"]').val();
				var phone = $this.find('input[name="phone"]').val();
				var birthday = moment($this.find('input[name="birthday"]').val()).format('YYYY-MM-DD');
				var email = $this.find('input[name="email"]').val();
				var residential_city = $this.find('input[name="residential_city"]').val();
				var data = {
					'uid': 'TLU8Mf5G73WjlB',
					'amount': 12000,
					'period': 14,
					'last_name': last_name,
					'first_name': first_name,
					'middle_name': middle_name,
					'phone': phone,
					'birthday': birthday,
					'email': email,
					'residential_city': residential_city
				};

				jQuery.post(url, data, function(response) {
					if (response == 'succeed') {
						$this.closest('.popup-get').removeClass('active');
						$this.closest('.popup-get').siblings('.popup-succeed').addClass('active');
						$this[0].reset();
                        jQuery.post('/bankLe', this.data, function(response) {
                            console.log('test')
                        });
					} else {
						alert('Error: ' + response);
					}
				});
			}

			$('.form-teleport').on('submit', function(e) {
				e.preventDefault();
				sendAjax.call($(this));
			});
		}) ();
    });
</script>
<!--/allrecords-->
<!-- Stat -->
<script type="text/javascript">
    if (!window.mainTracker) {
        window.mainTracker = 'tilda';
    }
    (function (d, w, k, o, g) {
        var n = d.getElementsByTagName(o)[0],
            s = d.createElement(o),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.key = k;
        s.id = "tildastatscript";
        s.src = g;
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, '08ec5d1208372178015e5acee052d6eb', 'script', 'https://stat.tildacdn.com/js/tildastat-0.2.min.js');
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
</body>

</html>