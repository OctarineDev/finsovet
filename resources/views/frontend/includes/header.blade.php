<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ФИНСОВЕТ</title>
    <meta property="og:url" content="http://finsovet.online"/>
    <meta property="og:title" content="ФИНСОВЕТ"/>
    <meta property="og:description" content="Лидер в мире финансовых советов"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image"
          content="{{ asset('public/frontend/images/tild6235-6265-4934-b461-653864626566__image-1.png') }}"/>
    <meta property="fb:app_id" content="257953674358265"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <meta name="description" content="Лидер в мире финансовых советов"/>
    <link rel="canonical" href="http://finsovet.online/">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon"
          href="{{ asset('public/frontend/images/tild3238-6461-4538-a261-396532393939__favicon1.ico') }}"
          type="image/x-icon"/>
    <!-- Assets -->
    <link rel="stylesheet" href="{{ asset ('public/frontend/css/tilda-grid-3.0.min.css') }}" type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{ asset ('public/frontend/css/tilda-blocks-2.12.css?t=1538124608') }}" type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{ asset ('public/frontend/css/tilda-animation-1.0.min.css') }}" type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{ asset ('public/frontend/css/tilda-slds-1.4.min.css') }}" type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{ asset ('public/frontend/css/tilda-zoom-2.0.min.css') }}" type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{ asset ('public/frontend/css/tilda-custom.css') }}" type="text/css"
          media="all"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
    <script src="{{ asset ('public/frontend/js/jquery-1.10.2.min.js') }}"></script>
	<script src="{{ asset ('public/frontend/js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-scripts-2.8.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-blocks-2.7.js?t=1538124608') }}"></script>
    <script src="{{ asset ('public/frontend/js/lazyload-1.3.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-animation-1.0.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-slds-1.4.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/hammer.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-zoom-2.0.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-forms-1.0.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-map-1.0.min.js') }}"></script>
    <script src="{{ asset ('public/frontend/js/tilda-cart-1.0.min.js') }}"></script>
    <script type="text/javascript">
        window.dataLayer = window.dataLayer || [];
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144787643-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-144787643-1');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(54640126, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/54640126" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>

<body class="t-body" style="margin:0;">
<!--allrecords-->
<div id="allrecords" class="t-records" data-hook="blocks-collection-content-node" data-tilda-project-id="770194"
     data-tilda-page-id="3252387" data-tilda-formskey="93ca36e356c225cbe8ec115f290da922">
    <div id="rec62604975" class="r t-rec" style=" " data-animationappear="off" data-record-type="456">
        <!-- T456 -->
        <div id="nav62604975marker"></div>
        <div id="nav62604975" class="t456 t456__positionstatic " style="background-color: rgba(255,255,255,1);"
             data-bgcolor-hex="#ffffff" data-bgcolor-rgba="rgba(255,255,255,1)" data-navmarker="nav62604975marker"
             data-appearoffset="" data-bgopacity-two="" data-menushadow="" data-bgopacity="1"
             data-menu-items-align="right" data-menu="yes">
            <div class="t456__maincontainer " style="">
                <div class="t456__leftwrapper" style="position: relative">
                    <a href="/" style="display: block;"><img
                                src="{{ asset('public/frontend/images/tild6235-6265-4934-b461-653864626566__image-1.png') }}"
                                class="t456__imglogo t456__imglogomobile" imgfield="img"
                                style="max-width: 230px; width: 230px;" alt="ФИНСОВЕТ">
                    </a>
                    <div class="menu-adaptive__burger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="t456__rightwrapper t456__menualign_right menu-adaptive" style="">
                    <ul class="t456__list">
                        <li class="t456__list_item">
                            <a class="t-menu__link-item" href="/#rec61198875" style="color:#000000;font-weight:600;"
                               data-menu-item-number="1">
                                О НАС
                            </a>
                        </li>
                        <li class="t456__list_item">
                            <a class="t-menu__link-item" href="/#rec61198876" style="color:#000000;font-weight:600;"
                               data-menu-item-number="2">
                                КАК ЭТО РАБОТАЕТ?
                            </a>
                        </li>
                        <li class="t456__list_item">
                            <a class="t-menu__link-item" href="/#rec61291558" style="color:#000000;font-weight:600;"
                               data-menu-item-number="3">
                                ПРОДУКТЫ
                            </a>
                        </li>
                        <li class="t456__list_item">
                            <a class="t-menu__link-item js-popup-button" href="#" data-popupShow="popup-partner"
                               style="color:#000000;font-weight:600;">
                                ПАРТНЕРАМ
                            </a>
                        </li>
                        <li class="t456__list_item">
                            <a class="t-menu__link-item" href="tel:88043330503" style="color:#000000;font-weight:600;"
                               data-menu-item-number="4">
                                8 (804) 333 05 03
                            </a>
                        </li>
                        @if(!Auth::guard('customer')->check())
                            <li class="t456__list_item">
                                <a href="/customer/login" class="t-menu__link-item t-menu__link-item--custom"
                                   style="color:#000000;font-weight:600;">
                                    <img src="{{ asset('public/frontend/images/lk.svg') }}" alt="">
                                    <span>ВОЙТИ</span>
                                </a>
                            </li>
                        @else
                            <li class="t456__list_item">
                                <a href="/customer/profile" class="t-menu__link-item t-menu__link-item--custom"
                                   style="color:#000000;font-weight:600;">
                                    <img src="{{ asset('public/frontend/images/lk.svg') }}" alt="">
                                    <span>ЛИЧНЫЙ КАБИНЕТ</span>
                                </a>
                            </li>
                            <li class="t456__list_item">
                                <a
                                        href="{{ route('logout') }}"
                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                        class="t-menu__link-item"
                                        style="color:#000000;font-weight:600;">
                                    ВЫЙТИ
                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <style>
            @media screen and (max-width: 980px) {
                #rec62604975 .t456__leftcontainer {
                    padding: 20px;
                }
            }

            @media screen and (max-width: 980px) {
                #rec62604975 .t456__imglogo {
                    padding: 20px 0;
                }
            }

            .t706__product-plus {
                display: none;
            }

            .t706__product-minus {
                display: none;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                t456_highlight();

                $('.menu-adaptive__burger').on('click', function () {
                    var menu = $('.menu-adaptive');
                    if (menu.hasClass('active')) return menu.removeClass('active');

                    menu.addClass('active');
                });
            });
            $(window).resize(function () {
                t456_setBg('62604975');
            });
            $(document).ready(function () {
                t456_setBg('62604975');
            });
        </script>
        <!--[if IE 8]>
        <style>#rec62604975 .t456 {
            filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#D9ffffff', endColorstr='#D9ffffff');
        }</style><![endif]-->
    </div>
@yield('content')
@extends('frontend.includes.footer')
