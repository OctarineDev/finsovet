<!-- pdf.blade.php -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>

<style type="text/css">

    body {
        font-size: 12px;
        font-family: georgia;
        font-weight: normal;
    }

    table {
        font-family: georgia;
        font-weight: normal;
        border-collapse: collapse;
        width: 100%;
    }

    .head td {
        border: 1px solid #a5a5a5;
        font-weight: bold;
        text-align: right;
        padding: 0 5px;
    }

    .head .td-img {
        text-align: left;
        padding: 0 5px 0 50px;
    }

    .main {
        margin: 30px 0 80px;
    }

    .heading {
        font-size: 24px;
        margin-bottom: 30px;
    }

    ul {
        margin: 0;
        padding: 0;
    }

    .info li {
        display: block;
        font-weight: bold;
        margin-bottom: 15px;
    }

    .red {
        color: #ff0000;
    }

    .center {
        text-align: center;
    }

    .bold {
        font-weight: bold;
    }

    .main-table p {
        margin: 0;
        margin-top: 10px;
    }

    .main-table thead {
        text-align: center;
    }

    .main-table thead td {
        background-color: #c1c1c1;
        vertical-align: top;
    }

    .main-table td {
        border: 1px solid #000000;
        padding: 0 5px;
    }

    .text-help {
        margin-top: 30px;
    }

    .text-help p {
        margin: 0;
    }

    .help-list {
        padding-left: 12px;
        margin-bottom: 15px;
    }

    @font-face {
        font-family: 'georgia';
        font-weight: normal;
        src: url( storage_path('fonts/georgia.ttf')  );
    }

    @font-face {
        font-family: 'georgia';
        font-weight: bold;
        src: url( storage_path('fonts/georgiabold.ttf')  );
    }

</style>

<!-- head -->
<div class="head">
    <table>
        <tr>
            <td rowspan="2" class="td-img">
                <img src="http://finsovet.online/public/frontend/images/logo.jpg" alt="logo">
            </td>
            <td>8 (804) 333 0503</td>
        </tr>
        <tr>
            <td>finsovet.online</td>
        </tr>
    </table>
</div>

<div class="main">
    <p class="heading center bold">Сертификат</p>
    <ul class="info">
        <li>
            <span class="info-title">Полное имя (ФИО): </span>
            <span class="info-text">{{ $order_for_pdf->name }}</span>
        </li>
        <li>
            <span class="info-title">Номер сертификата: </span>
            <span class="info-text">{{ $order_for_pdf->token_1 }}</span>
        </li>
        <li>
            <span class="info-title">Выбранный тарифный план: </span>
            <span class="info-text">{{ $order_for_pdf->type_1 }}</span>
        </li>
        <li>
            <span class="info-title">Дата оформления: </span>
            <span class="info-text">{{ date('d.m.Y', strtotime($order_for_pdf->created_at)) }}</span>
        </li>
        <li>
            <span class="info-title">Срок действия: </span>
            <span class="info-text">12 месяцев</span>
        </li>
        <li>
            <span class="info-title">Стоимость: </span>
            <span class="info-text">{{ $order_for_pdf->price_1 }} рублей</span>
        </li>
    </ul>
    <div class="main-table">
        <p class="bold">Об услугах:</p>
        <table>
            <thead>
            <tr>
                <td>Услуги</td>
                <td>Включены в <br> пакет</td>
            </tr>
            </thead>
            <tr>
                <td style="text-align: left;">
                    Количество пользователей
                </td>
                <td style="text-align: center;">
                    1
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    Устная консультация по сбережениям
                </td>
                <td style="text-align: center;">
                    +
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    Устная консультация по инвестициям
                </td>
                <td style="text-align: center;">
                    +
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    Устная консультация по кредитованию/кредитному здоровью
                </td>
                <td style="text-align: center;">
                    +
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    Устная консультация по страхованию
                </td>
                <td style="text-align: center;">
                    +
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    Ежемесячные практические советы по email
                </td>
                <td style="text-align: center;">
                    -
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    Всего устных консультаций в год по телефону
                </td>
                <td style="text-align: center;">
                    3
                </td>
            </tr>
        </table>
    </div>
    <div class="text-help">
        <p class="bold">Как воспользоваться сертификатом:</p>
        <p>Ваш сертификат активируется сразу после оплаты. После <br> активации Вам в течение одного календарного года доступны все услуги, <br> включенные в выбранный тарифный план. Если у вас возник вопрос, <br> разобраться в котором могут помочь специалисты службы «Финансовый <br> советник», Вы можете:</p>
        <ul class="help-list">
            <li>Позвонить нам по многоканальному номеру: 8 (804) 333 0503</li>
        </ul>
        <p>С публичной офертой, а также с правилами оказания услуг Вы можете <br> ознакомиться на сайте finsovet.online</p>
    </div>
</div>
</body>
</html>