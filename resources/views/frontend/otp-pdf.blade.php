<!-- pdf.blade.php -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>

<style type="text/css">

    body {
        font-size: 13px;
        font-family: georgia;
        font-weight: normal;
    }

    table {
        font-family: georgia;
        font-weight: normal;
    }

    .head {
        text-align: right;
    }

    .bottom-line {
        border-bottom: 1px solid #000;
        padding: 0 5px;
    }

    .center {
        text-align: center;
    }

    .indent-jus {
        text-indent: 1.5em;
        text-align: justify;
    }
    p{
        margin-top: 0.8em;
        margin-bottom: 0.8em;
    }
    .bold {
        font-weight: bold;
    }

    .jus {
        text-align: justify;
    }

    @font-face {
        font-family: 'georgia';
        font-weight: normal;
        src: url({{ storage_path('fonts/georgia.ttf') }});
    }

    @font-face {
        font-family: 'georgia';
        font-weight: bold;
        src: url({{ storage_path('fonts/georgiabold.ttf') }});
    }

</style>

<!-- head -->
<div class="head">
    {{--<div class="h-block bold">--}}
    {{--<span>В</span> <span class="bottom-line">55555555555555555555555555</span> <br>--}}
    {{--</div>--}}
    <div class="h-block">
        <span>Генеральному директору</span>
    </div>
    <div class="h-block">
        <span>{{ $row[3] }}</span>
    </div>
    <p></p>
    <div class="h-block">
        <span>От: Ассоциации Трудящихся По Защите Исполнения Работодателем Условий Труда</span>
    </div>
    <div class="h-block">
        <span>Руководитель: Афанасьев Илья Валерьевич</span>
    </div>
    <p></p>
    <div class="h-block">
        <span>Копия в: Государственную Инспекцию Труда. (электронно)</span>
    </div>

</div>
<br>
<br>
<br>
<br>
<div class="main">
    <p class="center">Запрос об исполнении заявления работника.</p>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p class="indent-jus">
        С {{ $row['date'] }} {{ $row[1] }} является полноправным членом нашей Ассоциации.
        Наша некоммерческая организация создана с целью реализации программы по свободному выбору работниками
        финансового учреждения удобного для получения ими заработной платы.
    </p>
    <p class="indent-jus">
        Сотрудник {{ $row[3] }} {{ $row[1] }} через Почту России подал заявление о перечислении заработной
        платы на счет в кредитно-финансовой организации на основании ст. 136 ТК РФ.
    </p>
    <p class="indent-jus">
        Просим предоставить информацию о надлежащем исполнении заявления работника (полноправного члена нашей ассоциации)
        о перечислении заработной платы на счет в кредитно-финансовой организации на основании ст. 136 ТК РФ.<br>
        В случае, если на работника оказывалось давление для отказа от поданного им заявления, то Ассоциация будет вынуждена
        принять меры контроля в соответствии с положениями Федерального законодательства.
    </p>
    <p class="indent-jus">
        За отказ от перевода зарплаты работника на указанные им реквизиты предусмотрена Административная ответственность организации,
        а также должностных лиц согласно действующему законодательству. Контроль за исполнением и наложение административного взыскания
        за неисполнение заявления работника осуществляет Роструд.
    </p>
    <p class="indent-jus">
        На основании изложенного, прошу:
    </p>
    <p class="indent-jus">
        - В срок не позднее 7 календарных дней с даты получения настоящего запроса предоставить сведения об исполнении заявления,
        которое направил {{ $row[1] }}, по перечислению заработной платы на счет указанной работником кредитно-финансовой организации.
        Ответ просим предоставить на электронную почту inspektor@trud-nko.ru или на почтовый адрес организации: 125252, г. Москва, Ходынский б-р, д. 2, эт. 4, пом. А-052
    </p>
    <br>
    <br>
    <p>
        С уважением,<br>
        Руководитель Ассоциации<br>
        {{--<span style="">--}}
        {{--<img src="{{ asset('public/frontend/images/otp_2.png') }}" alt="Logo" height="75px">--}}
        {{--<img src="{{asset('public/frontend/images/otp_1.png')}}" alt="Logo" height="150px">--}}
        {{--</span>--}}
        {{--<br>--}}
        Афанасьев Илья Валерьевич<br>
        {{ date('d.m.Y') }}

    </p>
</div>
</body>
</html>