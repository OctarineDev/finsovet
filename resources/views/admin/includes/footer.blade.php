<!--end::Page Scripts -->
</div>
<script type="application/javascript" src="{{ asset('public/admin/assets/vendors/base/vendors.bundle.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/demo/default/base/scripts.bundle.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
{{--<script type="application/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>--}}
<script type="application/javascript" src="{{ asset('public/admin/assets/app/js/jquery.maskedinput.min.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/js/app.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/vendors/custom/datatables/datatables.bundle.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/demo/default/custom/crud/datatables/basic/basic.js') }}"></script>
{{--<script>--}}
    {{--$("input[name='phone']").mask("+7 (999) 999-99-99");--}}
{{--</script>--}}

<!-- end::Body -->
</html>