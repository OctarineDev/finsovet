<?php


use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;



class AdminsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Admin::create( [
            'name' => 'Радик' ,
            'email' => 'rozhden.t@gmail.com' ,
            'role' => 'admin' ,
            'password' => Hash::make( 'asdasfwe45324tggw234f3' ),
        ] );

        App\Models\Admin::create( [
            'name' => 'Анатолий',
            'email' => '1sakhno.tolik@gmail.com' ,
            'role' => 'admin' ,
            'password' => Hash::make( 'asdasfwe45324tggw234f3' ),
        ] );

        App\Models\Admin::create( [
            'name' => 'Олег',
            'email' => 'prodavecmacdonalds@gmail.com' ,
            'role' => 'admin' ,
            'password' => Hash::make( 'asdasfwe45324tggw234f3' ),
        ] );

        App\Models\Admin::create( [
            'name' => 'Александр',
            'email' => 'a.babushkin@freshadvice.ru' ,
            'role' => 'admin' ,
            'password' => Hash::make( 'asdasfwe45324tggw234f3' ),
        ] );
    }
}
