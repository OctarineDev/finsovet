<?php

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Task;
use App\Models\Certificate;
use Carbon\Carbon;

Route::post('/cc/v1/getCallData', function (Request $request) {

    if($request->certificate){
        $certificate = $request->certificate;

        /*
         * Кусок г кода для сравни.ру
         */
        if(Certificate::where('name', $certificate)->first()){
            Order::create([
                'token_1' => $certificate,
                'type_1' => 'Сравни',
                'ko' => 'Сравни',
                'status' => 'Оплачен'
            ]);
        }

        $reso_OTP = mb_stripos($request->certificate, 'ОТР');
        $reso_OTPP = mb_stripos($request->certificate, 'ОТП'); // ОТП
        $reso_HC = mb_stripos($request->certificate, 'НС');
        $reso_PP = mb_stripos($request->certificate, 'ПР'); // ПР
        $start_token_number = substr($request->certificate, 0, -6);
        $start_token_number_alfa_1 = substr($request->certificate, 0, 4);
        $start_token_number_alfa_2 = substr($request->certificate, 0, 9);
        $check_rgs_certificate = Order::where('token_1', $request->certificate)->first();
        if($start_token_number == '5973' || $start_token_number == '5970'){
            if(!$check_rgs_certificate){
                Order::create([
                    'token_1' => $certificate,
                    'type_1' => 'РГС 1 ус. конс.',
                    'ko' => 'РГС',
                    'status' => 'Оплачен'
                ]);
            }
        }elseif($start_token_number_alfa_1 == '3951' || $start_token_number_alfa_1 == '3952' || $start_token_number_alfa_1 == '3953' || $start_token_number_alfa_1 == '3954' || $start_token_number_alfa_1 == '3955' ||
            $start_token_number_alfa_2 == 'z69223671' || $start_token_number_alfa_2 == 'z69223672' || $start_token_number_alfa_2 == 'z69223673' ||
            $start_token_number_alfa_2 == 'z69223674' || $start_token_number_alfa_2 == 'z69223675'){
            if(!$check_rgs_certificate) {
                if($start_token_number_alfa_1 == '3951') {
                    $type_1 = 'Мультиполис';
                }elseif ($start_token_number_alfa_1 == '3952'){
                    $type_1 = 'Мультиполис';
                }elseif ($start_token_number_alfa_1 == '3953'){
                    $type_1 = 'Мультиполис';
                }elseif ($start_token_number_alfa_1 == '3954'){
                    $type_1 = 'Мультиполис';
                }elseif ($start_token_number_alfa_1 == '3955'){
                    $type_1 = 'Мультиполис';
                }elseif ($start_token_number_alfa_2 == 'z69223671'){
                    $type_1 = 'Страхование имущества #1';
                }elseif ($start_token_number_alfa_2 == 'z69223672'){
                    $type_1 = 'Страхование имущества #2';
                }elseif ($start_token_number_alfa_2 == 'z69223673'){
                    $type_1 = 'Страхование имущества #3';
                }elseif ($start_token_number_alfa_2 == 'z69223674'){
                    $type_1 = 'Страхование имущества #4';
                }elseif ($start_token_number_alfa_2 == 'z69223675'){
                    $type_1 = 'Страхование имущества #5';
                }
                Order::create([
                    'token_1' => $certificate,
                    'type_1' => $type_1,
                    'ko' => 'Альфа',
                    'status' => 'Оплачен'
                ]);
            }
        }elseif($reso_OTP !== false && $reso_HC !== false ||
            $reso_OTPP !== false && $reso_HC !== false ||
            $reso_OTP !== false && $reso_PP !== false ||
            $reso_OTPP !== false && $reso_PP !== false
        ){
            //"ОТР и НС" или "ОТР и ПР" или "ОТП и НС" или "ОТП и ПР" то надо давать ответ
            // с name: РЕСО-ОТП type: РЕСО-ОТП count: 0/1

            Order::create([
                'token_1' => $certificate,
                'type_1' => 'РЕСО-ОТП',
                'ko' => 'РЕСО-ОТП',
                'status' => 'Оплачен'
            ]);
        }
        if($certificate){
            $data = Order::where('token_1', $certificate)->get();

            if($data != '[]'){
                $tasks = Task::where('token', $data[0]->id)->where('type', 'Устная')->orderByDesc('created_at')->get();
                $tasks_wr = Task::where('token', $data[0]->id)->where('type', 'Письменная')->get();
                $tasks_pr = Task::where('token', $data[0]->id)->where('type', 'Фин. план')->get();
                $newsletter = 'Нет';
                $limit = 'Ограничение на количество обращений в месяц отсутствует';

                if($data[0]->type_1 == 'Премиум'){
                    $count = 12;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'Базовый'){
                    $count = 6;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'Расширенный'){
                    $count = 10;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'РГС 1 ус. конс.'){
                    $count = 1;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'ОТП-кредитное здоровье' || $data[0]->type_1 == 'Сравни'){
                    $count = 3;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'ФинСовет ЕЮС тестовый сертификат VIP'){
                    $count = 10000;
                    $count_wr = 5;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'Телемедицина для Животных МинБанк тестовый сертификат VIP' || $data[0]->type_1 == 'ФинСовет Россельхоз тестовый сертификат VIP' ||
                    $data[0]->type_1 == 'Телемедицина для Животных Россельхоз тестовый сертификат VIP' || $data[0]->type_1 == 'Дачный Вопрос Россельхоз тестовый сертификат VIP' ||
                    $data[0]->type_1 == 'ФинСовет USAFE тестовый сертификат VIP' || $data[0]->type_1 == 'ФинСовет СРАВНИ.РУ тестовый сертификат VIP'){
                    $count = 1;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->ko == 'Альфа'){
                    if($tasks != '[]'){
                        $last_task = $tasks[0]->created_at;
                        $now  = Carbon::now();
                        $end  =  $tasks[0]->created_at;
                        if($now->diffInDays($end) < 30){
                            $result = 30-$now->diffInDays($end);
                            $limit = 'Лимит обращений в этом месяце исчерпан, до слежующего обращения осталось '.$result.' к.д.';
                        }
                    }
                    if($data[0]->type_1 == 'Мультиполис'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($data[0]->type_1 == 'Страхование имущества #1'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($data[0]->type_1 == 'Страхование имущества #2'){
                        $count = 3;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($data[0]->type_1 == 'Страхование имущества #3'){
                        $count = 7;
                        $count_wr = 1;
                        $count_fp = 0;
                        $newsletter = 'Да';
                    }elseif($data[0]->type_1 == 'Страхование имущества #4'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 0;
                        $newsletter = 'Да';
                    }elseif($data[0]->type_1 == 'Страхование имущества #5'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 1;
                        $newsletter = 'Да';
                    }
                }elseif ($data[0]->type_1 == 'РЕСО-ОТП'){
                    $count = 1;
                    $count_wr = 0;
                    $count_fp = 0;
                }

                if($count <= count($tasks)){
                    $limit = 'Лимит устных консультаций исчерпан';
                }
                $data = $data->map(function($items) use ($count, $tasks, $tasks_wr, $tasks_pr,$count_wr,$count_fp,$newsletter,$limit){
                    $data['name'] = $items->surname.' '.$items->name.' '.$items->second_name;
                    $data['email'] = $items->email;
                    $data['phone'] = $items->phone;
                    $data['type'] = $items->type_1;
                    $data['credit_organization'] = $items->ko;
                    $data['certificate'] = $items->token_1;
                    $data['count'] = count($tasks).'/'.$count;
                    $data['count_wr'] = count($tasks_wr).'/'.$count_wr;
                    $data['count_fp'] = count($tasks_pr).'/'.$count_fp;
                    $data['newsletter'] = $newsletter;
                    $data['birthday'] = $items->birthday;
                    $data['limit'] = $limit;
                    $data['place_of_birth'] = $items->place_of_birth;
                    $data['id_document_number'] = $items->id_document_number;
                    $data['identity_document_series'] = $items->identity_document_series;
                    $data['identity_date_document'] = $items->identity_date_document;
                    $data['name_of_issuing_authority'] = $items->name_of_issuing_authority;
                    $data['tin'] = $items->tin;
                    $data['address_register'] = $items->address_register;
                    $data['street_register'] = $items->street_register;
                    $data['address_of_live'] = $items->address_of_live;
                    $data['street_of_live'] = $items->street_of_live;
                    $data['ki'] = $items->ki;
                    return $data;
                });
                return $data;
            }else{
                return response([
                    'error' => 'Данного номера сертификата не существует',
                ], 422);
            }
        }
    }
    $phone = str_replace('+', '',$request->phone);

    if($phone){
        $data = Order::where('phone', $phone)->get();
        if($data != '[]'){
            if(count($data) > 1){
                return response([
                    'error' => 'За данным номером телефона закреплено больше одного сертфиката, укажите номер сертфиката',
                ], 422);
            }else{
                $tasks = Task::where('token', $data[0]->id)->where('type', 'Устная')->orderByDesc('created_at')->get();
                $tasks_wr = Task::where('token', $data[0]->id)->where('type', 'Письменная')->get();
                $tasks_pr = Task::where('token', $data[0]->id)->where('type', 'Фин. план')->get();
                $newsletter = 'Нет';
                $limit = 'Ограничение на количество обращений в месяц отсутствует';

                if($data[0]->type_1 == 'Премиум'){
                    $count = 12;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'Базовый'){
                    $count = 6;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'Расширенный'){
                    $count = 10;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->type_1 == 'РГС 1 ус. конс.'){
                    $count = 1;
                    $count_wr = 0;
                    $count_fp = 0;
                }elseif($data[0]->ko == 'Альфа'){
                    if($tasks != '[]'){
                        $last_task = $tasks[0]->created_at;
                        $now  = Carbon::now();
                        $end  =  $tasks[0]->created_at;
                        if($now->diffInDays($end) < 30){
                            $result = 30-$now->diffInDays($end);
                            $limit = 'Лимит обращений в этом месяце исчерпан, до слежующего обращения осталось '.$result.' к.д.';
                        }
                    }
                    if($data[0]->type_1 == 'Мультиполис'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($data[0]->type_1 == 'Страхование имущества #1'){
                        $count = 1;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($data[0]->type_1 == 'Страхование имущества #2'){
                        $count = 3;
                        $count_wr = 0;
                        $count_fp = 0;
                    }elseif($data[0]->type_1 == 'Страхование имущества #3'){
                        $count = 7;
                        $count_wr = 1;
                        $count_fp = 0;
                        $newsletter = 'Да';
                    }elseif($data[0]->type_1 == 'Страхование имущества #4'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 0;
                        $newsletter = 'Да';
                    }elseif($data[0]->type_1 == 'Страхование имущества #5'){
                        $count = 10;
                        $count_wr = 2;
                        $count_fp = 1;
                        $newsletter = 'Да';
                    }
                }
                if($count >= count($tasks)){
                    $limit = 'Лимит устных консультаций исчерпан';
                }
                $data = $data->map(function($items) use ($count, $tasks, $tasks_wr, $tasks_pr,$count_wr,$count_fp,$newsletter,$limit){
                    $data['name'] = $items->surname.' '.$items->name.' '.$items->second_name;
                    $data['email'] = $items->email;
                    $data['phone'] = $items->phone;
                    $data['type'] = $items->type_1;
                    $data['credit_organization'] = $items->ko;
                    $data['certificate'] = $items->token_1;
                    $data['count'] = count($tasks).'/'.$count;
                    $data['count_wr'] = count($tasks_wr).'/'.$count_wr;
                    $data['count_fp'] = count($tasks_pr).'/'.$count_fp;
                    $data['newsletter'] = $newsletter;
                    $data['limit'] = $limit;
                    $data['birthday'] = $items->birthday;
                    $data['place_of_birth'] = $items->place_of_birth;
                    $data['id_document_number'] = $items->id_document_number;
                    $data['identity_document_series'] = $items->identity_document_series;
                    $data['identity_date_document'] = $items->identity_date_document;
                    $data['name_of_issuing_authority'] = $items->name_of_issuing_authority;
                    $data['tin'] = $items->tin;
                    $data['address_register'] = $items->address_register;
                    $data['street_register'] = $items->street_register;
                    $data['address_of_live'] = $items->address_of_live;
                    $data['street_of_live'] = $items->street_of_live;
                    $data['ki'] = $items->ki;
                    return $data;
                });
                return $data;
            }
        }else{
            return response([
                'error' => 'За данным номером телефона не закреплен сертфикат, укажите номер сертификата',
            ], 422);
        }
    }
});
