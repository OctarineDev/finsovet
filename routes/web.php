<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {


//    for ($i = 1; $i <= 12000; $i++) {
//        $number  = rand(1, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9);
//        if(!\App\Models\Certificate::where('name', $number)->first()){
//            \App\Models\Certificate::create(['name'=>$number]);
//            $i++;
//        }
//    }
    return view('frontend.home');
});

Route::get('/test-connect-with-otp/date={date}', 'OtpController@order');
Route::get('/test-connect-with-otp-status-update', 'OtpController@statusUpdate');
Route::get('/test-ks-for-tg', 'CustomerController@testKsForTg');

Route::get('/pm', function () {
    return response()->download(public_path('Памятка__Кредитное_Здоровье_.pdf'));
});

Route::get('/cl=otps', function () {
    return view('frontend.bank-phone');
});
Route::get('/cl={type}', function ($type) {
    if($type == 'otpc'){
        $price = 144;
    }elseif($type == 'otpe'){
        $price = 199;
    }elseif($type == 'otpve'){
        $price = 269;
    }elseif($type == 'dom'){
        $price = 269;
    }elseif($type == 'le'){
        return view('frontend.bank-le');
    }else{
        abort(404);
    }
    return view('frontend.bank', ['type'=> $type, 'price' => $price]);
});


Route::post('/order', 'OrderController@order');
Route::post('/order-type-4', 'OrderController@orderNewType')->name('order.type.4');
Route::post('/checkOrder', 'OrderController@checkOrder');
Route::post('/bankLe', 'OrderController@bankLe');
Route::get('/test', 'OrderController@test');
Route::post('/api/updateOrder', 'Admin\OrderController@apiUpdateOrder');
Route::post('/api/confirm', 'Admin\OrderController@confirm');

Route::get('/kr', function () {
    return view('frontend.confirm-ks');
});
Route::post('/confirm-ks', 'CustomerController@getKs')->name('confirm.ks');

Route::get('/customer/login', 'Auth\CustomerLoginController@showLoginForm')->name('customer.login');
Route::post('/login', 'Auth\CustomerLoginController@login')->name('customer.login.submit');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/customer/password/reset', 'Auth\CustomerForgotPasswordController@showLinkRequestForm')->name('customer.password.request');
Route::post('/customer/password/email', 'Auth\CustomerForgotPasswordController@sendResetLinkEmail')->name('customer.password.email');
Route::post('/customer/password/reset', 'Auth\CustomerResetPasswordController@reset');
Route::get('/customer/password/reset/{token}', 'Auth\CustomerResetPasswordController@showResetForm')->name('customer.password.reset');

Route::post('/confirm', 'CustomerController@confirm')->name('customer.confirm');
Route::get('/confirm', function () {
    return view('frontend.customer-confirm');
});
Route::group(['middleware' => ['customer']], function () {

    Route::prefix('customer')->group(function () {

        Route::get('/profile', function () {
            return view('frontend.customer.profile');
        });

        Route::get('/type={type}/id={id}', function ($type,$id) {
            $type = $type;
            $order =  App\Models\Order::where('id', $id)->first();
            if($order->email == Auth::guard('customer')->user()->email){
                return view('frontend.customer.confirm-nbki', ['id' => $id, 'type' => $type]);
            }else{
                return view('frontend.home');
            }
        });

        Route::get('/get-ks', function () {
            return view('frontend.customer.confirm-ks');
        });

        Route::get('/print/type={type}/id={id}', 'CustomerController@print');

        Route::get('/get-history/id={id}', 'CustomerController@getHistory' );

        Route::post('/confirm-nbki', 'CustomerController@confirmNbki')->name('customer.confirm.nbki');
        Route::post('/confirm-ks', 'CustomerController@getKs')->name('customer.confirm.ks');
        Route::get('/test', 'CustomerController@test');
        Route::get('/test1', 'CustomerController@test1');

    });

});


Route::prefix('admin')->group(function () {
    Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::group(['middleware' => ['admin']], function () {

        /*
        * Order
        */
        Route::post('/orders', function () {
            $orders = App\Models\Order::orderBy('id', 'DESC')->get();
            $orders->load('tasks');
            return $orders;
        });
        Route::get('/orders', function () {
            return view('admin.order');
        });
        Route::post('/order/create', 'Admin\OrderController@create');
        Route::post('/order/filter', 'Admin\OrderController@filter');
        Route::post('/order/delete/id={id}', 'Admin\OrderController@delete');

        /*
        * Task
        */
        Route::post('/tasks', function () {
            $tasks = App\Models\Task::orderBy('id', 'DESC')->get();
            $tasks->load('order');
            $tasks->load('admin');
            foreach($tasks as $task){
                $task->add_admin_id = $task->admin->email;
                $task->token = $task->order->token_1;
            }
            return $tasks;
        });
        Route::get('/tasks', function () {

            return view('admin.task');
        });
        Route::post('/token-list', 'Admin\TaskController@getToken');
        Route::post('/admin-list', 'Admin\TaskController@getAdmin');
        Route::post('/task/create', 'Admin\TaskController@create');
        Route::post('/task/filter', 'Admin\TaskController@filter');
        Route::post('/task/send-message', 'Admin\TaskController@sendMessage');
        Route::post('/task/delete/id={id}', 'Admin\TaskController@delete');


        /*
        * Admins
        */
        Route::post('/admins', function () {
            $admins = App\Models\Admin::orderBy('id', 'DESC')->get();
            return $admins;
        });
        Route::get('/admins', function () {
            return view('admin.admin');
        });
        Route::post('/admin/create', 'Admin\AdminController@create');
        Route::post('/admin/delete/id={id}', 'Admin\AdminController@delete');

        /*
      * Task stat
      */
        Route::post('/task-stats', function () {
            $orders = App\Models\Order::orderBy('id', 'DESC')->get();
            $orders->load('tasks');
            foreach($orders as $order){
                foreach($order->tasks as $task){
                    $task->load('admin');
                }
            }
            return $orders;
        });
        Route::get('/task-stats', function () {
            return view('admin.task-stat');
        });
        Route::post('/task/filter-stat', 'Admin\TaskController@filterStat');

        Route::get('/bz-admins', function () {
            return view('admin.bz-admin');
        });

        Route::get('/bz-all', function () {
            return view('admin.bz-all');
        });

    });
});

